﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MercurySoldier
{
    public static class VectorExtensions
    {
        public static void ClampMagnitude(this ref Vector3 v, float maxMagnitude)
        {
            float curMagnitude = Vector3.Distance(Vector3.Zero, v);
            if(curMagnitude > maxMagnitude)
            {
                v = v / curMagnitude * maxMagnitude;
            }
        }

        public static void ClampComponents(this ref Vector3 v, float maxMagnitude)
        {
            if (v.X > maxMagnitude)
                v.X = maxMagnitude;
            if (v.Y > maxMagnitude)
                v.Y = maxMagnitude;
            if (v.Z > maxMagnitude)                
                v.Z = maxMagnitude;           
        }
    }
}
