﻿using System;

using Microsoft.Xna.Framework;

namespace MercurySoldier
{
    public static class QuaternionExtensions
    {
        public static void GetEulerAngles(this Quaternion quaternion, out float x, out float y, out float z)
        {           
            double tmpY = (float)Math.Asin(2 * (quaternion.W * quaternion.X - quaternion.Z * quaternion.Y));
            y = (float)tmpY;
            double piHalf = Math.PI / 2;
            if (tmpY == piHalf)
            {                
                x = 0;
                z = -2 * (float)Math.Atan2(quaternion.Z, quaternion.W);
                return;
            } 
            else if (tmpY == -piHalf)
            {                
                x = 0;
                z = 2 * (float)Math.Atan2(quaternion.Z, quaternion.W);
                return;
            }
            x = (float)Math.Atan2(2 * (quaternion.W * quaternion.Z + quaternion.X * quaternion.Y), quaternion.W * quaternion.W - quaternion.Z * quaternion.Z - quaternion.X * quaternion.X + quaternion.Y * quaternion.Y);
            z = (float)Math.Atan2(2 * (quaternion.W * quaternion.Y + quaternion.Z * quaternion.X), quaternion.W * quaternion.W + quaternion.Z * quaternion.Z - quaternion.X * quaternion.X - quaternion.Y * quaternion.Y);
        }
    }
}
