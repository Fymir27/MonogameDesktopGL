﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DefaultEcs;

namespace MercurySoldier
{
    using Components;
    using System;

    struct Blueprint
    {
        public static Systems.RigidBodySystem RigidBodySystem;
        public static Systems.ParticleSystem ParticleSystem;

        public string Name;
        public Texture2D Texture;
        public Color? Tint;
        public Point SpriteSize;
        public SpriteRenderer.DrawMode DrawMode;
        public bool RigidBody;
        public float RigidBodyMass;
        public bool HitBox;
        public int HitBoxType;
        public float ParticleForce;

        public Blueprint(string name, Texture2D texture, Color? tint = null)
        {
            Name = name;
            Texture = texture;
            Tint = tint;
            SpriteSize = texture.Bounds.Size;
            DrawMode = SpriteRenderer.DrawMode.Simple;
            RigidBody = false;
            RigidBodyMass = 1f;
            HitBox = false;
            HitBoxType = 0;
            ParticleForce = 0f;
        }

        public void InitEntity(ref Entity entity)
        {            
            var renderer = new SpriteRenderer(Texture, SpriteSize, Tint ?? Color.White, DrawMode);
            entity.Set(renderer);

            var localMatrix = entity.Get<Transform>().LocalMatrix;            

            entity.Set(this);
            
            if (RigidBody) 
            {
                RigidBodySystem.InitializeBody(entity, RigidBodyMass, Vector3.Zero);
            }
            
            if (HitBox)
            {               
                entity.Set(new HitBox(entity, HitBoxType));
            }

            if (ParticleForce > 0)
            {
                var pos = localMatrix.GetTranslation();
                var location = new Point((int)MathF.Round(pos.X), (int)MathF.Round(pos.Y)) - renderer.Center.ToPoint();
                var quaternion = localMatrix.GetRotation();
                quaternion.GetEulerAngles(out float x, out float y, out float z);
                var rotation = quaternion.W;
                var direction = new Vector2(MathF.Cos(2 * MathF.PI + x), MathF.Sin(2 * MathF.PI + x));
                ParticleSystem.AddLocalForce(new Rectangle(location, SpriteSize), direction * ParticleForce);
            }

            // TODO: add other components
        }
    }
}