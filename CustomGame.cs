﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System;
using System.Collections.Generic;
using System.Linq;

using DefaultEcs;

namespace MercurySoldier
{
    using Components;    
    using Systems;

    public class CustomGame : Game
    {
        public static SpriteFont DefaultFont;

        private GraphicsDeviceManager _graphics;

        // update time needs to be counted manually, because 
        // monogame doesn't support independent draw/update
        private TimeSpan timeSinceLastUpdate;
        private int targetUpdateRate = 30;

        private Scene curScene;
        private RenderSystem renderSystem;
        private RotationSystem rotationSystem;
        private ControlSystem controlSystem;
        private ParticleSystem particleSystem;
        private RigidBodySystem rigidBodySystem;
        private CollisionDetectionSystem collisionDetection;
        private MassSpringSystem massSpringSystem;

        private Entity player;
        private Entity rootSpiral;
        private List<Entity> playerSpirals = new List<Entity>();
        private Entity plane1;
        private Entity plane;
        private Entity test1, test2, test3, test4;


        private int lastScrollWheelValue;
        private bool savePressed;

        private RigidBody[] Bodies;        
        
        private Keys[] keysPressedLastUpdate = { };        


        public enum Form : int
        {
            Actor,
            Ball, 
            Cloud,
            Cone
        }

        public CustomGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            
            // unlock framerate           
            IsFixedTimeStep = false;
        }
        
        protected override void Initialize()
        {
            base.Initialize();
            var backgroundTexture = Content.Load<Texture2D>("textures/background");
            var ballTexture = Content.Load<Texture2D>("textures/orb6");
            var particleTexture = Content.Load<Texture2D>("textures/particle");
            var arrowTexture = Content.Load<Texture2D>("textures/arrow");
            var spiralTexture = Content.Load<Texture2D>("textures/spiral");
            var groundTexture = Content.Load<Texture2D>("textures/ground");
            var wallTexture = Content.Load<Texture2D>("textures/wall");
            var fanTexture = Content.Load<Texture2D>("textures/fan");
            var soldierTexture = Content.Load<Texture2D>("textures/rook");

            FormHandler.FormTextures = new Dictionary<Shape, Texture2D>()
            {
                [Shape.Actor] = soldierTexture,
                [Shape.Ball] = ballTexture,
            };

            Scene.AddBlueprint(new Blueprint("ball", ballTexture)
            {
                RigidBody = true,
                RigidBodyMass = 3f,
                HitBox = true,
                HitBoxType = 1
            });
            Scene.AddBlueprint(new Blueprint("ground", groundTexture)
            {
                DrawMode = SpriteRenderer.DrawMode.Tiled,
                SpriteSize = groundTexture.Bounds.Size + new Point(128, 0),
                HitBox = true,
                HitBoxType = 0
            });
            Scene.AddBlueprint(new Blueprint("wall", wallTexture)
            {
                DrawMode = SpriteRenderer.DrawMode.Tiled,
                SpriteSize = wallTexture.Bounds.Size + new Point(0, 128),
                HitBox = true,
                HitBoxType = 0
            });
            Scene.AddBlueprint(new Blueprint("fan", fanTexture)
            {                                
                ParticleForce = 500f
            });

            Scene.AddBlueprint(new Blueprint("background", backgroundTexture)
            {
                DrawMode = SpriteRenderer.DrawMode.Tiled,
                SpriteSize = backgroundTexture.Bounds.Size + new Point(1000, 0),
            });
            Scene.AddBlueprint(new Blueprint("particle", particleTexture));

            rigidBodySystem = new RigidBodySystem(new Vector3(0, 0.5f, 0));
            Blueprint.RigidBodySystem = rigidBodySystem;
            particleSystem = new ParticleSystem();
            Blueprint.ParticleSystem = particleSystem;

            var jsonContent = System.IO.File.ReadAllText(Content.RootDirectory + "/scenes/testScene.json");
            curScene = Scene.FromJson(jsonContent);
            
            renderSystem = new RenderSystem(GraphicsDevice);
            renderSystem.DebugArrowTexture = arrowTexture;
            renderSystem.ParticleSystem = particleSystem;
            renderSystem.SetParticleTexture(particleTexture);
            rotationSystem = new RotationSystem(curScene.World);                        
            collisionDetection = new CollisionDetectionSystem();
            massSpringSystem = new MassSpringSystem(5);
            
            // gravity
            particleSystem.AddGlobalForce(new Vector2(0, 100f));                                                                

            // adds Transform automatically
            #region Player Instanziating Test
            /*
            player = curScene.CreateEntity(Vector3.Up * 50);
            player.Set(new SpriteRenderer(Content.Load<Texture2D>("textures/ball")));
            player.Set(new ContinuousRotation() { RotationsPerSecond = .1f });
            player.Get<Transform>().LocalMatrix.SetScale(new Vector3(3, 3, 1));            

            var satellite1 = curScene.CreateEntity(Vector3.Right * 50, player);
            satellite1.Set(new SpriteRenderer(Content.Load<Texture2D>("textures/ball")));
            satellite1.Set(new ContinuousRotation() { RotationsPerSecond = .2f });
            satellite1.Get<Transform>().LocalMatrix.SetScale(new Vector3(.5f, .5f, 1));

            var satellite2 = curScene.CreateEntity(Vector3.Right * 30, satellite1);
            satellite2.Set(new SpriteRenderer(Content.Load<Texture2D>("textures/ball")));
            satellite2.Set(new ContinuousRotation() { RotationsPerSecond = .3f });
            satellite2.Get<Transform>().LocalMatrix.SetScale(new Vector3(.5f, .5f, 1));
            */
            #endregion

            #region Testing Rigid Body Dynamics
            Texture2D rect = new Texture2D(_graphics.GraphicsDevice, 500, 100);
            Color[] data = new Color[500 * 100];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Black;
            rect.SetData(data);


            // define planes
            /*
            plane = curScene.CreateEntity(new Vector3(-100, 500, 5));
            plane.Set(new SpriteRenderer(rect));
            plane.Set(new HitBox(plane, 0));

            plane1 = curScene.CreateEntity(new Vector3(300, 400, 5));
            plane1.Set(new SpriteRenderer(rect));
            plane1.Set(new HitBox(plane1, 0));
            */

            Texture2D circel = massSpringSystem.createCircleText(60, _graphics.GraphicsDevice);

            // define player
            Texture2D tinyRect = new Texture2D(_graphics.GraphicsDevice, 100, 100);
            data = new Color[100 * 100];
            for (int i = 0; i < data.Length; ++i) data[i] = Color.Green;
            tinyRect.SetData(data);

            var playerPos = new Vector3(-50, 0, 5);
            player = curScene.CreateEntity(playerPos);
            //player = curScene.CreateEntity(new Vector3(-50, -350, 5));
            player.Set(new SpriteRenderer(ballTexture));
            //player.Set(new SpriteRenderer(tinyRect));
            player.Set(new FormHandler((int)Form.Ball));
            player.Set(new HitBox(player, 1));

            // spirals around player to showcase hierarchichal transformations            
            rootSpiral = Spiral(player, Vector3.Zero, spiralTexture, 4);
            curScene.SetEnabledRecursively(rootSpiral, false);

            // define more balls
            /*var ball1 = curScene.CreateEntity(new Vector3(70, -150, 5));
            //test1.Set(new SpriteRenderer(circel));
            ball1.Set(new SpriteRenderer(ballTexture));
            ball1.Set(new FormHandler((int)Form.Ball));
            ball1.Set(new HitBox(ball1, 1));*/

            /* test2 = curScene.CreateEntity(new Vector3(-400, -150, 5));
             //test2.Set(new SpriteRenderer(circel));
             test2.Set(new SpriteRenderer(ballTexture));
             test2.Set(new FormHandler((int)Form.Ball));
             test2.Set(new HitBox(test2, 1));

             test3 = curScene.CreateEntity(new Vector3(400, -250, 5));
             //test3.Set(new SpriteRenderer(circel));
             test3.Set(new SpriteRenderer(ballTexture));
             test3.Set(new FormHandler((int)Form.Ball));
             test3.Set(new HitBox(test3, 1));*/

            // -----------------------------------------------------------------------
            // Build Bridge
            Entity stick1 = curScene.CreateEntity(new Vector3(0, 0, 0));
            Entity stick2 = curScene.CreateEntity(new Vector3(0, 0, 0));
            Entity stick3 = curScene.CreateEntity(new Vector3(0, 0, 0));
            Entity stick4 = curScene.CreateEntity(new Vector3(0, 0, 0));
            Entity stick5 = curScene.CreateEntity(new Vector3(0, 0, 0));

            test1 = curScene.CreateEntity(new Vector3(-800, 100, 5));
            test1.Set(new SpriteRenderer(circel));
            test1.Set(new HitBox(test1, 1));
            rigidBodySystem.InitializeBody(test1, 0f, new Vector3(0, 0, 0), true);

            test2 = curScene.CreateEntity(new Vector3(0, 100, 5));
            test2.Set(new SpriteRenderer(circel));
            test2.Set(new HitBox(test2, 1));
            rigidBodySystem.InitializeBody(test2, 0f, new Vector3(0, 0, 0), true);

            test3 = curScene.CreateEntity(new Vector3(-375, 150, 5));
            test3.Set(new SpriteRenderer(circel));
            test3.Set(new HitBox(test3, 1));
            rigidBodySystem.InitializeBody(test3, 2f, new Vector3(0, 0, 0));

            test4 = curScene.CreateEntity(new Vector3(-525, 150, 5));
            test4.Set(new SpriteRenderer(circel));
            test4.Set(new HitBox(test4, 1));
            rigidBodySystem.InitializeBody(test4, 2f, new Vector3(0, 0, 0));

            var test5 = curScene.CreateEntity(new Vector3(-250, 150, 5));
            test5.Set(new SpriteRenderer(circel));
            test5.Set(new HitBox(test5, 1));
            rigidBodySystem.InitializeBody(test5, 2f, new Vector3(0, 0, 0));

            // define stick
            stick1.Set(new Stick(ref test1, ref test4, ref stick1));
            stick2.Set(new Stick(ref test4, ref test3, ref stick2));
            stick3.Set(new Stick(ref test3, ref test5, ref stick3));
            stick4.Set(new Stick(ref test5, ref test2, ref stick4));

            massSpringSystem.AddStick(stick1.Get<Stick>());
            massSpringSystem.AddStick(stick2.Get<Stick>());
            massSpringSystem.AddStick(stick3.Get<Stick>());
            massSpringSystem.AddStick(stick4.Get<Stick>());//*/

            // -----------------------------------------------------------------------

            rigidBodySystem.InitializeBody(player, 10f, new Vector3(-3, -10, 0));
            rigidBodySystem.ControlledEntity = player;
            //rigidBodySystem.InitializeBody(ball1, 10f, new Vector3(5, 0, 0));
            //rigidBodySystem.InitializeBody(test2, 10f, new Vector3(-5, 0, 0));
            //rigidBodySystem.InitializeBody(test3, 10f, new Vector3(2, 0, 0));//*

   
            controlSystem = new ControlSystem(player, 1.5f);
            controlSystem.RigidBodySystem = rigidBodySystem;
            #endregion

            // Set Application Title
            Window.Title = "Mercury Soldier";

            // Custom Game Resolution
            _graphics.PreferredBackBufferWidth = 1024;
            _graphics.PreferredBackBufferHeight = 768;
            _graphics.ApplyChanges();

            Window.AllowUserResizing = true;     

            // for now particles are independent of their "transform" position
            //var particleContainer = curScene.CreateEntity(Vector3.Zero);
            int particleCount = 100;
            var particles = new Particle[particleCount];
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].M = 1;
            }

            var playerParticles = new ParticleCollection()
            {
                Particles = particles,
                StickTogether = true
            };
            player.Set(playerParticles);
            particleSystem.SetPlayerParticles(playerParticles);
            player.Disable<ParticleCollection>();
        }

        private readonly Color[] spiralColors =
        {
            Color.Blue,
            Color.Red,
            Color.Yellow,
            Color.Green,
            Color.Black,
        };

        private Entity Spiral(Entity parent, Vector3 offset, Texture2D spiralTexture, int layersLeft)
        {
            if (layersLeft == 0)            
                return new Entity(); // never used
            
            var spiral = curScene.CreateEntity(offset, parent);
            playerSpirals.Add(spiral);
            var renderer = new SpriteRenderer(spiralTexture, new Color(spiralColors[layersLeft % spiralColors.Length], .2f))
            {
                Layer = .1f // slightly in front of everything
            };
            spiral.Set(renderer);
            spiral.Set(new ContinuousRotation() { RotationsPerSecond = layersLeft / 5f });
            Spiral(spiral, new Vector3(30, 0, 0), spiralTexture, layersLeft - 1);
            Spiral(spiral, new Vector3(-30, 0, 0), spiralTexture, layersLeft - 1);
            return spiral;
        }

        protected override void LoadContent()
        {
            DefaultFont = Content.Load<SpriteFont>("defaultFont");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();

            // only execute physics update if certain time has passed 
            // = "Fixed" update (almost) independent of framerate
            timeSinceLastUpdate += gameTime.ElapsedGameTime;
            int targetMS = 1000 / targetUpdateRate;            
            renderSystem.DisplayedTargetUpdateMS = targetMS;

            if (timeSinceLastUpdate.TotalMilliseconds < targetMS)
                return;

            // Updating all objects
            float deltaTime = timeSinceLastUpdate.Milliseconds / 1000f;
            renderSystem.DisplayedUpdateMS = timeSinceLastUpdate.Milliseconds;

            HandleBlueprintControls();

            if (controlSystem.KeyJustPressed(Keys.P))
            {
                if (player.IsEnabled<ParticleCollection>())
                {
                    player.Disable<ParticleCollection>();
                }
                else
                {
                    player.Enable<ParticleCollection>();
                }
            }

            if (controlSystem.KeyJustPressed(Keys.D5))
            {
                renderSystem.drawMassSpringDebugger *= -1;
            }
            if (controlSystem.KeyJustPressed(Keys.D6))
            {
                renderSystem.drawRigidBodyDebugger *= -1;
            }
            if (controlSystem.KeyJustPressed(Keys.D7))
            {
                curScene.SetEnabledRecursively(rootSpiral, !rootSpiral.IsEnabled());
            }
            if (controlSystem.KeyJustPressed(Keys.D8))
            {
                particleSystem.MethodOfIntegration = ParticleSystem.IntegrationMethod.Euler;
            }
            if (controlSystem.KeyJustPressed(Keys.D9))
            {
                particleSystem.MethodOfIntegration = ParticleSystem.IntegrationMethod.RungeKutta;
            }
            if (controlSystem.KeyJustPressed(Keys.D0))
            {
                renderSystem.DrawDebugGrid = !renderSystem.DrawDebugGrid;
            }

            if (controlSystem.KeyJustPressed(Keys.Add))
            {
                targetUpdateRate++;
            }

            if (controlSystem.KeyJustPressed(Keys.Subtract))
            {
                targetUpdateRate--;
            }

            // Console.WriteLine($"Update: {timeSinceLastUpdate.TotalSeconds}");
            timeSinceLastUpdate = TimeSpan.Zero;
            
            //collisionDetection.UpdateHitBoxIdentifierPosition(test4, test3, player);

            //collisionDetection.CheckCollisions(CollisionDetectionSystem.Direction.Vertical, curScene, player);
            controlSystem.Update(deltaTime);

            // TODO: get player movement speed
            var inputForce = controlSystem.Direction * 30f;
            if (controlSystem.JumpPressed)
            {
                inputForce += Vector3.Down * 400f;
            }

            particleSystem.ControlParticles(new Vector2(controlSystem.Direction.X, controlSystem.Direction.Y));
            rigidBodySystem.SetPlayerInputForce(inputForce);
            rigidBodySystem.Update(deltaTime, curScene);
            collisionDetection.Update(curScene);

            if (controlSystem.KeyJustPressed(Keys.Up))
            {
                massSpringSystem.UpdateSpringConstant(1);
            }
            if (controlSystem.KeyJustPressed(Keys.Down))
            {
                massSpringSystem.UpdateSpringConstant(-1);
            }

            massSpringSystem.Update(deltaTime);

            particleSystem.Update(curScene, deltaTime);
            rotationSystem.Update(gameTime, curScene);
            RotateDebugSpirals(gameTime);

            keysPressedLastUpdate = Keyboard.GetState().GetPressedKeys();
            base.Update(gameTime);
        }

        private void RotateDebugSpirals(GameTime gameTime)
        {            
            foreach (var entity in playerSpirals)
            {
                var rotationDefinition = entity.Get<ContinuousRotation>();
                var transform = entity.Get<Transform>();
                double rotationsThisFrame = gameTime.ElapsedGameTime.TotalSeconds * rotationDefinition.RotationsPerSecond;
                double angle = rotationsThisFrame * Math.PI * 2;
                transform.LocalMatrix.SetRotation(transform.LocalMatrix.GetRotation() * Quaternion.CreateFromAxisAngle(Vector3.Backward, (float)angle));
            }
        }

        private void HandleBlueprintControls()
        {
            var keyboard = Keyboard.GetState();

            if (controlSystem.KeyJustPressed(Keys.B))
            {
                curScene.StartBlueprintMode();
            }

            if (controlSystem.KeyJustPressed(Keys.Escape))
            {
                curScene.StopBlueprintMode();
            }

            var mouse = Mouse.GetState();
            var mouseScreenPos = new Vector2(mouse.X, mouse.Y);
            var mouseWorldPos = renderSystem.ScreenToWorldPosition(mouseScreenPos);
            var rotationsPerTick = .05f;
            var mouseScrollPerTick = 120; // TODO: test on other mice than my own
            var newAngle = (mouse.ScrollWheelValue / mouseScrollPerTick) * rotationsPerTick * MathF.PI * 2;
            curScene.UpdateCurrentBlueprintPosition(new Vector3(mouseWorldPos, 0f), newAngle);

            if (controlSystem.KeyJustPressed(Keys.N))
            {
                curScene.ChangeBlueprint(true);
            }                        

            if (controlSystem.KeyJustPressed(Keys.V))
            {
                curScene.ChangeBlueprint(false);
            }

            if (mouse.LeftButton == ButtonState.Pressed)
            {
                curScene.PlaceBlueprint();
            }

            if (keyboard.IsKeyDown(Keys.LeftControl) && controlSystem.KeyJustPressed(Keys.S))
            {
                var timestamp = DateTime.Now;
                string sceneJson = curScene.ToJson();
                string filename = "scene" /*_" + timestamp.ToString("yyy-MM-dd_HH-mm-ss") */ + ".json";
                System.IO.File.WriteAllText(filename, sceneJson);             
            }

        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Peru);
            // Console.WriteLine($"Draw: {gameTime.ElapsedGameTime.TotalSeconds}");

            Vector2 pos2D;
            if (player.Get<FormHandler>().currentForm == (int)Shape.Cloud)
            {
                pos2D = player.Get<ParticleCollection>().AveragePosition();
            }
            else
            {
                var pos3D = player.Get<Transform>().LocalMatrix.GetTranslation();
                pos2D = new Vector2(pos3D.X, pos3D.Y);
            }
            
            renderSystem.RenderScene(gameTime, curScene, pos2D);

            base.Draw(gameTime);
        }
    }
}
