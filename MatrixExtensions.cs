﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

namespace MercurySoldier
{
    public static class MatrixExtensions
    {        
        public static Vector3 GetTranslation(this Matrix matrix)
        {
            _ = matrix.Decompose(out _, out _, out var translation);
            return translation;
        }

        public static Quaternion GetRotation(this Matrix matrix)
        {
            _ = matrix.Decompose(out _, out var rotation, out _);
            return rotation;
        }

        public static Vector3 GetScale(this Matrix matrix)
        {
            _ = matrix.Decompose(out var scale, out _, out _);
            return scale;
        }

        // already implemented by Monogame, just adding this method for completeness
        public static void SetTranslation(this ref Matrix matrix, Vector3 translation)
        {            
            matrix.Translation = translation;
        }     

        public static void SetRotation(this ref Matrix matrix, Quaternion rotation)
        {
            _ = matrix.Decompose(out var scale, out _, out var translation);
            matrix = Matrix.CreateScale(scale) * Matrix.CreateFromQuaternion(rotation) * Matrix.CreateTranslation(translation);            
        }

        public static void SetScale(this ref Matrix matrix, Vector3 scale)
        {
            _ = matrix.Decompose(out var _, out var rotation, out var translation);
            matrix = Matrix.CreateScale(scale) * Matrix.CreateFromQuaternion(rotation) * Matrix.CreateTranslation(translation);
        }

        public static string Pretty(this Matrix matrix)
        {
            return
                $"{matrix.M11:F} {matrix.M12:F} {matrix.M13:F} {matrix.M14:F}\n" +
                $"{matrix.M21:F} {matrix.M22:F} {matrix.M23:F} {matrix.M24:F}\n" +
                $"{matrix.M31:F} {matrix.M32:F} {matrix.M33:F} {matrix.M34:F}\n" +
                $"{matrix.M41:F} {matrix.M42:F} {matrix.M43:F} {matrix.M44:F}";                
        }

        public static Matrix VectorToMatrix (Vector3 vector)
        {
            Matrix matrix = new Matrix(vector.X, 0, 0, 0, vector.Y, 0, 0, 0, vector.Z, 0, 0, 0, 0, 0, 0, 0);
            return matrix;
        }

        public static Vector3 MatrixToVector(Matrix matrix)
        {
            Vector3 vector = new Vector3(matrix.M11, matrix.M21, matrix.M31);
            return vector;
        }
    }
}
