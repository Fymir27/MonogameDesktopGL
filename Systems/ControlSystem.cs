﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


using DefaultEcs;
using DefaultEcs.System;

namespace MercurySoldier.Systems
{
    using Components;

    sealed class ControlSystem
    {
        public RigidBodySystem RigidBodySystem;

        private Entity Character;
        public float Speed;
        private int Form;
        public Vector3 Direction = Vector3.Zero;
        public bool JumpPressed = false;
        private KeyboardState currentKeyboardState;
        private KeyboardState lastKeyboardState;

        public ControlSystem(Entity character) : this(character, 1) { }


        public ControlSystem(Entity character, float speed)
        {
            Character = character;
            Speed = speed;
        }

        public bool KeyJustPressed(Keys key)
        {            
            return currentKeyboardState.IsKeyDown(key) && !lastKeyboardState.IsKeyDown(key);
        }

        public void Update(float deltaTime)
        {
            lastKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            SwitchForm();

            Form = Character.Get<FormHandler>().currentForm;

            // bug fix
            Character.Set(new ContinuousRotation() { RotationsPerSecond = 0f });
                        
            if (Form == 0)
            {                
                ActorControls();
            }
            else if(Form == 1)
            {
                BallControls(deltaTime);
            }
            else if (Form == 2)
            {                
                CloudControls();
            }
            else if (Form == 3)
            {                
                ConeControls();
            }            
        }

        private void SwitchFromCloud()
        {
            Character.Enable<SpriteRenderer>();
            Character.Enable<HitBox>();
            Character.Enable<RigidBody>();
            Character.Disable<ParticleCollection>();
            var averagePosition = Character.Get<ParticleCollection>().AveragePosition();            
            var newPos = new Vector3(averagePosition.X, averagePosition.Y, 0);
            Character.Get<RigidBody>().X = newPos;
            Character.Get<RigidBody>().oldX = newPos;
        }

        private void SwitchToCloud()
        {
            Character.Enable<ParticleCollection>();
            Character.Disable<SpriteRenderer>();
            Character.Disable<HitBox>();
            Character.Disable<RigidBody>();
            var playerParticles = Character.Get<ParticleCollection>();
            var anchor3D = Character.Get<Transform>().LocalMatrix.GetTranslation();
            var anchor = new Vector2(anchor3D.X, anchor3D.Y);
            var rng = new Random();
            for (int i = 0; i < playerParticles.Particles.Length; i++)
            {
                playerParticles.Particles[i].Pos = anchor + new Vector2(rng.Next(-50, 50), rng.Next(-50, 50));
            }
            playerParticles.ZeroVelocities();
        }

        private void SwitchForm()
        {
            Shape? possibleNewShape = null;
            if (currentKeyboardState.IsKeyDown(Keys.D1))
            {
                possibleNewShape = Shape.Actor;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.D2))
            {               
                possibleNewShape = Shape.Ball;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.D3))
            {
                possibleNewShape = Shape.Cloud;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.D4))
            {
                possibleNewShape = Shape.Cone;
            }

            var currentShape = (Shape)Character.Get<FormHandler>().currentForm;
            if (possibleNewShape == null || possibleNewShape == currentShape)
                return;

            var newShape = possibleNewShape.Value;

            Character.Set(new FormHandler(Convert.ToInt32(newShape)));

            if (FormHandler.FormTextures.ContainsKey(newShape))
            {
                Character.Set(new SpriteRenderer(FormHandler.FormTextures[newShape]));
                RigidBodySystem.InitializeBody(Character, Character.Get<RigidBody>().Mass, Vector3.Zero);
                Character.Set(new HitBox(Character, currentShape == Shape.Ball ? 1 : 0));             
            }

            if (newShape == Shape.Cloud)
            {
                SwitchToCloud();
            } 
            else if (currentShape == Shape.Cloud)
            {
                SwitchFromCloud();
            }

        }

        private void ActorControls()
        {
            // Movement Logic
            if (currentKeyboardState.IsKeyDown(Keys.A))
            {
                Direction = Vector3.Left;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.D))
            {
                Direction = Vector3.Right;
            }
            else
            {
                Direction = Vector3.Zero;
            }

            JumpPressed = KeyJustPressed(Keys.Space);
        }

        private void BallControls(float deltaTime)
        {
            Direction = Vector3.Zero;
            if (currentKeyboardState.IsKeyDown(Keys.A))
            {
                Direction = Vector3.Left;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.D))
            {
                Direction = Vector3.Right;
            }

            JumpPressed = KeyJustPressed(Keys.Space);

           

            #region test
                /*
                if (keyState.IsKeyDown(Keys.A))
                {
                    Direction.X = -0.707f;
                    Velocity.X += 100;
                }
                else if (keyState.IsKeyDown(Keys.D))
                {
                    Direction.X = 0.707f;
                    Velocity.X += 100;
                }

                if (Keyboard.GetState().IsKeyDown(Keys.Space) && hasJumped == false)
                {
                    //Position.Y -= 300f;
                    Direction.Y = -10f;
                    Velocity.Y = 100f;
                    hasJumped = true;
                }

                if (hasJumped == true)
                {
                    float i = 1;
                    Velocity.Y += 0.15f * i;
                    Direction.Y += 0.15f * i;
                }

                if (((Position.Y + Character.Get<SpriteRenderer>().Texture.Height) >= 600) && hasJumped == true)
                {
                    hasJumped = false;
                }

                if (hasJumped == false)
                {
                    Velocity.Y = 0f;
                    Direction.Y = 0f;
                }


                Position += Direction * Velocity * deltaTime;
                Character.Get<Transform>().LocalMatrix.SetTranslation(Position);
                //Character.Set(new ContinuousRotation() { RotationsPerSecond = (Direction.X * Velocity * deltaTime) / Character.Get<SpriteRenderer>().Texture.Width});
                Character.Set(new ContinuousRotation() { RotationsPerSecond = Direction.X});

                // slowing down movement over time
                Direction.X *= 0.94f;
                if ((keyState.GetPressedKeys() == null || keyState.GetPressedKeys().Length == 0) && Velocity.X > 800)
                {
                    //if no keys pressed at all. 
                    Velocity.X *= 0.95f;
                }*/
                #endregion

                /*
                Position += Velocity;

                // Movement Logic
                if (keyState.IsKeyDown(Keys.A))
                {
                    Velocity.X = -10f * Speed;
                }
                else if (keyState.IsKeyDown(Keys.D))
                {
                    Velocity.X = 10f * Speed;
                }
                else
                {
                    Velocity.X = 0f;
                }

                Jump(40f, 32f);

                Character.Get<Transform>().LocalMatrix.SetTranslation(Position);
                Character.Set(new ContinuousRotation() { RotationsPerSecond = (Velocity.X * 5) / (Character.Get<SpriteRenderer>().Texture.Width) });
                */
        }

        private void CloudControls()
        {
            if (currentKeyboardState.IsKeyDown(Keys.A))
            {
                Direction = Vector3.Left;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.D))
            {
                Direction = Vector3.Right;
            }
            else
            {
                Direction = Vector3.Zero;
            }

            if (currentKeyboardState.IsKeyDown(Keys.Space) || currentKeyboardState.IsKeyDown(Keys.W))
            {
                Direction.Y = -1;
            }
            else if (currentKeyboardState.IsKeyDown(Keys.LeftShift) || currentKeyboardState.IsKeyDown(Keys.S))
            {
                Direction.Y = 1;
            }
            else
            {
                Direction.Y = 0;
            }
        }

        private void ConeControls()
        {
            
        }
    }
}
