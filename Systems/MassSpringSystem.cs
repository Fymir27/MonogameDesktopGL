﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using CenterSpace.NMath.Core;

using DefaultEcs;
using DefaultEcs.System;

namespace MercurySoldier.Systems
{
	using Components;
	class MassSpringSystem
    {
		//private List<Stick> sticks = new List<Stick>();
		private Stick[] sticks = new Stick[0];

		public int k;

		public MassSpringSystem (int springConstant)
		{
			k = springConstant;
		}

		public void UpdateSpringConstant( int addToK)
        {
			int tmpK = k + addToK;
			
			if (tmpK > 0)
            {
				k = tmpK;
            }
        }


		public void Update(float deltaTime)
        {
			int counter = k;
			while (counter > 0)
			{
				for (var i = 0; i < sticks.Length; i++)
				{
					if ( counter == k)
                    {
						sticks[i].strain = 0f;
					}

					sticks[i].updateRigidBodies();
					float dx = sticks[i].rb1.X.X - sticks[i].rb0.X.X;
					float dy = sticks[i].rb1.X.Y - sticks[i].rb0.X.Y;
					float distance = (float)Math.Sqrt(dx * dx + dy * dy);
					float difference = sticks[i].length - distance;
					float percent = difference / distance / 2;

					if (percent != 0)
					{
						float offsetX = dx * percent;
						float offsetY = dy * percent;

						float offsetMultiplyer_rb0 = 1;
						float offsetMultiplyer_rb1 = 1;

						if (sticks[i].rb0.pinned)
							offsetMultiplyer_rb1 = 2;

						if (sticks[i].rb1.pinned)
							offsetMultiplyer_rb0 = 2;

						float dl_x = 0f;
						float dl_y = 0f;

						if (!sticks[i].rb0.pinned)
						{
							sticks[i].rb0.X.X -= offsetX * offsetMultiplyer_rb0;
							sticks[i].rb0.X.Y -= offsetY * offsetMultiplyer_rb0;

							// calc Hookean Force
							dl_x = sticks[i].dx - dx;
							dl_y = sticks[i].dy - dy;

							sticks[i].rb0.force += new Vector3(k * dl_x, k * dl_y, 0); 
						}

						if (!sticks[i].rb1.pinned)
						{
							sticks[i].rb1.X.X += offsetX * offsetMultiplyer_rb1;
							sticks[i].rb1.X.Y += offsetY * offsetMultiplyer_rb1;

							// calc Hookean Force
							dl_x = sticks[i].dx - dx;
							dl_y = sticks[i].dy - dy;

							sticks[i].rb1.force += new Vector3(k * dl_x, k * dl_y, 0);
						}

						sticks[i].strain += 20 * Math.Abs(percent);

						sticks[i].updateEntities();
					}

				}

				counter--;
			}

		}

		public void AddStick(Stick stick)
		{
			Array.Resize(ref sticks, sticks.Length + 1);
			sticks[sticks.GetUpperBound(0)] = stick;
		}

		public Stick[] getSticks()
		{
			return sticks;
		}

		public Texture2D createCircleText(int radius, GraphicsDevice _graphics)
		{
			Texture2D texture = new Texture2D(_graphics, radius, radius);
			Color[] colorData = new Color[radius * radius];

			float diam = radius / 2f;
			float diamsq = diam * diam;

			for (int x = 0; x < radius; x++)
			{
				for (int y = 0; y < radius; y++)
				{
					int index = x * radius + y;
					Vector2 pos = new Vector2(x - diam, y - diam);
					if (pos.LengthSquared() <= diamsq)
					{
						colorData[index] = Color.Red;
					}
					else
					{
						colorData[index] = Color.Transparent;
					}
				}
			}

			texture.SetData(colorData);
			return texture;
		}
	}
}
