﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using CenterSpace.NMath.Core;

using DefaultEcs;
using DefaultEcs.System;

namespace MercurySoldier.Systems
{
    using Components;
    sealed class RigidBodySystem
    {
        public Entity ControlledEntity;

        CollisionDetectionSystem collisionDetection;
        Vector3 inputForce;
        Vector3 globalForce;

        public RigidBodySystem(Vector3 force)
        {
            collisionDetection = new CollisionDetectionSystem();
            globalForce = force;
        }

        public void InitializeBody(Entity character, float mass, Vector3 initialForce, bool pinned = false)
        {
            character.Set(new RigidBody());
            RigidBody y0 = new RigidBody(mass);

            y0.width = character.Get<SpriteRenderer>().Texture.Width;
            y0.height = character.Get<SpriteRenderer>().Texture.Height;
            y0.X = character.Get<Transform>().LocalMatrix.GetTranslation();
            
            // set initial rotation
            y0.q.W = initialForce.X * -0.1f;
            y0.omega = 0f;

            y0.oldX.X = y0.X.X + initialForce.X;
            y0.oldX.Y = y0.X.Y + initialForce.Y;

            // Define if physics are applied
            y0.pinned = pinned;

            ComputeIinv(ref y0);

            if (character.Has<FormHandler>())
            {
                if (character.Get<FormHandler>().currentForm == 1) //Ball
                {
                    ComputeBallInertia(ref y0);
                }
                else
                {
                    ComputeBoxInertia(ref y0);
                }
            }
            else 
            {
                ComputeBoxInertia(ref y0);
            }

            character.Set<RigidBody>(y0);
        }


        public void Update(float ElapsedGameTime, Scene scene)
        {
            RigidBody y0 = new RigidBody(0f);
            RigidBody yVert = new RigidBody(0f);
            RigidBody yFinal = new RigidBody(0f);
            Entity character;

            foreach (var transform in scene.Root.Children)
            {    
                if (transform.Owner.Has<RigidBody>() && transform.Owner.IsEnabled<RigidBody>())
                {
                    character = transform.Owner;
                }
                else
                    continue;

                y0 = character.Get<RigidBody>();
                y0.force += inputForce;
                

                //trying velocity verlet
                float friction = 0.99f;

                // do not apply physics if body is pinned
                if (y0.pinned)
                {
                    continue;
                }

                y0.v.X = (y0.X.X - y0.oldX.X) * friction;
                y0.v.Y = (y0.X.Y - y0.oldX.Y) * friction;

                y0.v += character == ControlledEntity ? inputForce * ElapsedGameTime : Vector3.Zero;

                if ((inputForce.X > 0 && y0.q.W < 0) || (inputForce.X < 0 && y0.q.W > 0))
                    y0.q.W *= -1f;

                y0.oldX.X = y0.X.X;
                y0.oldX.Y = y0.X.Y;

                y0.X.X += y0.v.X;
                y0.X.Y += y0.v.Y;
                //Console.WriteLine("player on (" + y0.X.X + ", " + y0.X.Y + ")\n");

                y0.X += globalForce;

                y0.force = y0.v * y0.Mass;
                //Console.WriteLine("force: (" + y0.force.X + ", " + y0.force.Y + ")\n");
                y0.J = (y0.force + globalForce) * ElapsedGameTime;

                transform.Owner.Set<RigidBody>(y0);
                transform.LocalMatrix.SetTranslation(y0.X);
            }
        }

        public void UpdateGlobalForce(Vector3 force)
        {
            globalForce = force;
        }

        public void UpdateLocalForce(RigidBody rb, Vector3 force)
        {
            rb.force = force;
        }

        public void SetPlayerInputForce(Vector3 force)
        {
            inputForce = force;
        }

        public void ComputeOmega(RigidBody rb)
        {
            Matrix L_mat = MatrixExtensions.VectorToMatrix(rb.L);

            //rb.omega = MatrixExtensions.MatrixToVector(Matrix.Multiply(rb.Iinv, L_mat));
        }

        public void ComputeIinv(ref RigidBody rb)
        {
            Matrix temp = Matrix.Multiply(rb.R, rb.Ibodyinv);

            rb.Iinv = Matrix.Multiply(temp, Matrix.Invert(rb.R)); // correct? What does Invert with a quaternion?
        }

        public void ComputeForceAndTorque(ref RigidBody rb, float t)
        {
            //TODO: Take all forces (e.g. gravity) ínto account


            /* P_dot(t) = v_dot(t) / Mass = F(t) */
            /* v_dot(t) = x_dot_dot(t) */
            /* P(t) = M * v(t) */
            /* P(t) = M * a * t */
            /* P_dot(t) = M * a = F(t) */
            /* a = v(t) / t */

            /*Vector3 a = Vector3.Divide(rb.v, t);
            rb.force = Vector3.Multiply(a, rb.Mass);*/

            // Apply earths gravity 
            rb.force.Y += rb.Mass * 9.81f;

            // Apply wind
            rb.force.X += rb.Mass * 0.5f;

            rb.force += inputForce;

            /* T(t) = L_dot(t) = d/dt(I(t) * w(t)) */
            /* L = r x P whereby r is the position => r = X */
            /* L_dot(t) = d/dt(X) x P + X + d/dt(P) = 0 + X x P_dot = X x P_dot */
            /* T(t) = X x P_dot = T(t) = X x F(t) */

            //rb.torque = Vector3.Cross(rb.X, rb.force);


            Vector2 r = new Vector2(rb.width / 2, rb.height / 2);
            rb.torque = r.X * rb.force.Y - r.Y * rb.force.X;

        }

        // Calculates the inertia of a box shape and stores it in the momentOfInertia variable.
        public void ComputeBoxInertia(ref RigidBody boxShape)
        {
            float m = boxShape.Mass;
            float w = boxShape.width;
            float h = boxShape.height;
            float momentOfInertia = m * (w * w + h * h) / 12;
            boxShape.momentOfInertia = momentOfInertia;

            boxShape.Ibody.M11 = momentOfInertia;
            boxShape.Ibody.M22 = momentOfInertia;
            boxShape.Ibody.M33 = momentOfInertia;
        }

        public void ComputeBallInertia(ref RigidBody ballShape)
        {
            float m = ballShape.Mass;
            float r = ballShape.width / 2;
            float momentOfInertia =  m * (r * r) * 0.4f;
            ballShape.momentOfInertia = momentOfInertia;

            ballShape.Ibody.M11 = momentOfInertia;
            ballShape.Ibody.M22 = momentOfInertia;
            ballShape.Ibody.M33 = momentOfInertia;
        }

    }
}
