﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


using DefaultEcs;
using DefaultEcs.System;

namespace MercurySoldier.Systems
{
    using Components;
    sealed class RenderSystem
    {
        public Vector2 CameraPos = Vector2.Zero;

        // for local forces of particle systems
        public ParticleSystem ParticleSystem;
        public Texture2D DebugArrowTexture;
        public int DebugGridSize = 16;
        public bool DrawDebugGrid = false;

        public int DisplayedUpdateMS = 0;
        public int DisplayedTargetUpdateMS = 0;

        private GraphicsDevice graphics;
        private Vector2 screenCenter;
        private SpriteBatch spriteBatch;

        private Texture2D particleTexture;

        private GameTime currentGameTime;

        public int drawRigidBodyDebugger = -1;
        public int drawMassSpringDebugger = -1;


        public RenderSystem(GraphicsDevice graphics)
        {
            this.graphics = graphics;                        
            spriteBatch = new SpriteBatch(graphics);
        }

        public Vector2 ScreenToWorldPosition(Vector2 screenPos)
        {
            return CameraPos + screenPos - screenCenter;
        }

        public void SetParticleTexture(Texture2D tex)
        {
            // TODO: should this ever be set more than once/during game?
            particleTexture = tex;
        }

        public void RenderScene(GameTime gameTime, Scene scene, Vector2 playerPos)
        {
            CameraPos = playerPos;
            screenCenter = new Vector2(graphics.Viewport.Width / 2, graphics.Viewport.Height / 2);

            currentGameTime = gameTime;
            spriteBatch.Begin(SpriteSortMode.FrontToBack);

            foreach (var transform in scene.Root.Children)
            {
                if (transform.Owner.IsEnabled())
                    Draw(transform, Matrix.Identity);

            }

            if (DrawDebugGrid)
            {
                var referenceDirection = new Vector2(0, -1);
                var color = new Color(Color.Green, .2f);
                var windowSize = graphics.Viewport.Bounds.Size;
                for (int y = 0; y < windowSize.Y; y += DebugGridSize)
                {
                    for (int x = 0; x < windowSize.X; x += DebugGridSize)
                    {
                        var worldPos = CameraPos - screenCenter + new Vector2(x, y);
                        var force = ParticleSystem.GetLocalForceAt(worldPos);
                        if (force == Vector2.Zero)
                            continue;
                        var angle = MathF.Atan2(force.Y, force.X) + MathF.PI / 2; // referenceDirection) / force.Length());
                        spriteBatch.Draw(DebugArrowTexture, new Vector2(x, y), null, color, angle, DebugArrowTexture.Bounds.Center.ToVector2(), Vector2.One, SpriteEffects.None, 1f);
                    }
                }
            }

            spriteBatch.DrawString(CustomGame.DefaultFont, $"Frame time:  {gameTime.ElapsedGameTime.Milliseconds} ms", new Vector2(10, 10), Color.Blue);
            spriteBatch.DrawString(CustomGame.DefaultFont, $"Update time: {DisplayedUpdateMS, 2}/{DisplayedTargetUpdateMS, 2} ms (achieved/target)", new Vector2(10, 30), Color.Blue);

            spriteBatch.End();
        }

        private void Draw(Transform transform, Matrix parentGlobal)
        {
            var curGlobal = transform.LocalMatrix * parentGlobal;

            // draw myself before going deeper
            if (transform.Owner.Has<SpriteRenderer>() && transform.Owner.IsEnabled<SpriteRenderer>())
            {
                Draw(transform.Owner.Get<SpriteRenderer>(), curGlobal);
            }

            if(transform.Owner.Has<ParticleCollection>() && transform.Owner.IsEnabled<ParticleCollection>())
            {
                Draw(transform.Owner.Get<ParticleCollection>());
            }

            if (transform.Owner.Has<RigidBody>() && drawRigidBodyDebugger > 0)
            {

                VisualizeRigidBody(transform.Owner.Get<RigidBody>(), curGlobal, parentGlobal);
            }

            if (transform.Owner.Has<Stick>() )
            {
                var attachedEntityGlobal = transform.Owner.Get<Stick>().localMatrix * parentGlobal;
                DrawLine(transform.Owner.Get<Stick>(), attachedEntityGlobal);
                //DrawLine(transform.Owner.Get<Stick>(), curGlobal);
            }

            if (transform.Children == null)
                return;

            foreach(var childTransform in transform.Children)
            {
                Draw(childTransform, curGlobal);
            }
        }

        private void Draw(in SpriteRenderer renderer, in Matrix global)
        {
            var pos3D = global.GetTranslation();
            // we should negate Y coordinate because it's inverted in for screenspace (UP == DOWN),
            // but that would also invert rotations at every level in the hierarchy. TODO: fix at some point?            
            var pos2D = new Vector2(pos3D.X, pos3D.Y);
            var rot = global.GetRotation();
            var scale3D = global.GetScale();

            // we assume only rotations around z-axis are allowed
            float angle = 2f * (float)Math.Acos(rot.W) * Math.Sign(rot.Z);
            
            var screenPosition = new Point((int)(pos2D.X + screenCenter.X), (int)(pos2D.Y + screenCenter.Y)) - CameraPos.ToPoint();
            var screenSize = renderer.Size;
            screenSize.X = (int)MathF.Round(screenSize.X * scale3D.X);
            screenSize.Y = (int)MathF.Round(screenSize.Y * scale3D.Y);
            var dstRect = new Rectangle(screenPosition, screenSize);
            spriteBatch.Draw(renderer.Texture, dstRect, null, renderer.Tint, angle, renderer.Texture.Bounds.Size.ToVector2() / 2f, SpriteEffects.None, renderer.Layer);                  
        }

        private void Draw(in ParticleCollection particleCollection)
        {
            foreach(var particle in particleCollection.Particles)
            {
                spriteBatch.Draw(particleTexture, particle.Pos + screenCenter - CameraPos, null, particleCollection.Color);
            }            
        }

        public void VisualizeRigidBody(in RigidBody y0, in Matrix global, in Matrix parentGlobal)
        {
            Texture2D t;
            t = new Texture2D(graphics, 1, 1);
            t.SetData<Color>(new Color[] { Color.White });

            var pos3D = global.GetTranslation();
            var pos2D = new Vector2(pos3D.X, pos3D.Y);
            var screenPosition = new Point((int)(pos2D.X + screenCenter.X), (int)(pos2D.Y + screenCenter.Y)) - CameraPos.ToPoint();

            Vector2 edge = new Vector2(y0.X.X, y0.X.Y) - new Vector2(y0.oldX.X, y0.oldX.Y);
            // calculate angle to rotate line
            float angle = (float)Math.Atan2(edge.Y, edge.X);

            spriteBatch.Draw(t,
                new Rectangle(// rectangle defines shape of line and position of start of line

                    (int)screenPosition.X, (int)screenPosition.Y,
                    100, //sb will strech the texture to fill this rectangle
                    5), //width of line, change this to make thicker line
                null,
                Color.Red, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);

            /*if (y0.intersects)
            {
                var globalIntersection = new Matrix(1,0,0,0, 0,1,0,0, 0,0,1,0, y0.pointOfIntersection.X, y0.pointOfIntersection.Y, 0, 1) * parentGlobal;
                pos3D = globalIntersection.GetTranslation();
                pos2D = new Vector2(pos3D.X, pos3D.Y);
                screenPosition = new Point((int)(pos2D.X + screenCenter.X), (int)(pos2D.Y + screenCenter.Y)) - CameraPos.ToPoint();

                spriteBatch.Draw(t,
                new Rectangle(// rectangle defines shape of line and position of start of line

                    (int)screenPosition.X, (int)screenPosition.Y,
                    10, //sb will strech the texture to fill this rectangle
                    10), //width of line, change this to make thicker line
                null,
                Color.Red, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);
            }*/

            
        }

        public void DrawLine(in Stick stick, in Matrix global)
        {
            Texture2D t;
            t = new Texture2D(graphics, 1, 1);
            t.SetData<Color>(new Color[] { Color.White });

            var pos3D = global.GetTranslation();
            var pos2D = new Vector2(pos3D.X, pos3D.Y);
            var screenPosition = new Point((int)(pos2D.X + screenCenter.X), (int)(pos2D.Y + screenCenter.Y)) - CameraPos.ToPoint();
            //pos2D += screenCenter;

            Vector2 edge = new Vector2(stick.rb1.X.X, stick.rb1.X.Y) - new Vector2(stick.rb0.X.X, stick.rb0.X.Y);
            // calculate angle to rotate line
            float angle =
                (float)Math.Atan2(edge.Y, edge.X);


            Color stickColor = Color.Black;

            if (drawMassSpringDebugger > 0)
            {
                stickColor = new Color(0.0f + stick.strain, 1.0f - stick.strain, 0f);
            }


            spriteBatch.Draw(t,
                new Rectangle(// rectangle defines shape of line and position of start of line
                    /*(int)stick.rb0.X.X,
                    (int)stick.rb0.X.Y,*/
                    (int)screenPosition.X, (int)screenPosition.Y,
                    (int)edge.Length(), //sb will strech the texture to fill this rectangle
                    stick.width), //width of line, change this to make thicker line
                null,
                stickColor, //colour of line
                angle,     //angle of line (calulated above)
                new Vector2(0, 0), // point in line about which to rotate
                SpriteEffects.None,
                0);

        }
    }
}
