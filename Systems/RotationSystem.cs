﻿using System;

using Microsoft.Xna.Framework;

using DefaultEcs;
using DefaultEcs.System;

namespace MercurySoldier.Systems 
{
    using Components;
    class RotationSystem
    {
        private World world;

        public RotationSystem(World world)
        {
            this.world = world;
        }

        public void Update(GameTime gameTime, Scene scene)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            /*var entitySet = world.GetEntities().With(new[] {
                    typeof(ContinuousRotation),
                    typeof(Transform)
            });

            foreach (var entity in entitySet.AsEnumerable())
            {
                
                if (entity.Has<RigidBody>())
                {
                   RigidBody y0 = CalcRigidBodyRotation(entity.Get<RigidBody>(), deltaTime);
                   entity.Set<RigidBody>(y0);
                   entity.Set(new ContinuousRotation() { RotationsPerSecond = (y0.q.W) });
                }

                Update(gameTime, entity.Get<ContinuousRotation>(), entity.Get<Transform>());
            }*/

            foreach (var transform in scene.Root.Children)
            {
                if (transform.Owner.Has<RigidBody>())
                {
                    RigidBody y0 = CalcRigidBodyRotation(transform.Owner.Get<RigidBody>(), deltaTime);
                    transform.Owner.Set<RigidBody>(y0);
                    transform.Owner.Set(new ContinuousRotation() { RotationsPerSecond = (y0.q.W) });

                    Update(gameTime, transform.Owner.Get<ContinuousRotation>(), transform.Owner.Get<Transform>());
                }
            }
        }

        protected void Update(GameTime gameTime, in ContinuousRotation rotationDefinition, in Transform transform)
        {   
            double rotationsThisFrame = gameTime.ElapsedGameTime.TotalSeconds * rotationDefinition.RotationsPerSecond;            
            double angle = rotationsThisFrame * Math.PI * 2;
            transform.LocalMatrix.SetRotation(transform.LocalMatrix.GetRotation() * Quaternion.CreateFromAxisAngle(Vector3.Backward, (float)angle));            
        }

        private RigidBody CalcRigidBodyRotation(RigidBody y0, float ElapsedGameTime)
        {
            RigidBody yfinal = y0;

            float distanceX = Math.Abs(y0.X.X - y0.oldX.X);
            float d = y0.width;

            float numberOfRotations = (distanceX / d);
            float omegaTmp = (float)(2 * Math.PI * numberOfRotations);

            /*if(Math.Abs(y0.q.W * d) < Math.Abs(omegaTmp * ElapsedGameTime))
            {
                y0.q.W = omegaTmp * 100 * ElapsedGameTime;
            }*/



            if (Math.Abs(y0.v.X) > 0.5f || Math.Abs(y0.v.Y) > 2f)
            {
                Vector2 acceleration = new Vector2(y0.v.X / ElapsedGameTime, y0.v.Y / ElapsedGameTime);
                //Vector2 force = new Vector2(acceleration.X * y0.Mass, acceleration.Y * y0.Mass);
                Vector2 force = new Vector2 (y0.J.X / ElapsedGameTime, y0.J.Y / ElapsedGameTime);

                Vector2 r = new Vector2(y0.width / 2, y0.height / 2);
                y0.torque = r.X * force.Y - r.Y * force.X;

                float angularAcceleration = y0.torque / y0.momentOfInertia;
                if((y0.v.X < 0 && angularAcceleration > 0) || (y0.v.X > 0 && angularAcceleration < 0))
                {
                    angularAcceleration *= -1f;
                }

                y0.omega = angularAcceleration * ElapsedGameTime;

                //y0.q.W = -((y0.omega * ElapsedGameTime) - y0.q.W);
                //y0.q.W += 0.5f * y0.omega * y0.q.W; 

                y0.q.W += y0.omega * ElapsedGameTime;
            }
            else if(Math.Abs(y0.v.X) > 0.2f || Math.Abs(y0.v.Y) > 2f)
            {
                
                y0.omega = (float)(2 * Math.PI * numberOfRotations);

                y0.q.W += y0.omega * ElapsedGameTime;
            }
            else
            {
                y0.q.W *= 0.99f;
            }

            yfinal = y0;

            return yfinal;
        }
    }
}
