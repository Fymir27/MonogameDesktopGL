﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using CenterSpace.NMath.Core;

using DefaultEcs;
using DefaultEcs.System;

namespace MercurySoldier.Systems
{
    using Components;

    public struct Collision
    {        
        public Vector2 Point;
        public Vector2 Normal;
        public override string ToString()
        {
            return $"P:{Point}, N:{Normal}";
        }
    }  
    
    sealed class CollisionDetectionSystem
    {

        private List<Projection> projectedEntities = new List<Projection>();

        public CollisionDetectionSystem()
        {

        }

        public void Update(Scene scene)
        {
            int counter = 0;
            projectedEntities.Clear();

            foreach (var transform in scene.Root.Children)
            {
                if (transform.Owner.Has<HitBox>() && transform.Owner.IsEnabled<HitBox>())
                {
                    UpdateHitBox(transform.Owner);

                    // Add all entities to projection array for broad check
                    projectedEntities.Add(new Projection(ref transform.Owner, counter, "y", false));
                    projectedEntities.Add(new Projection(ref transform.Owner, counter, "y", true));
                    counter++;
                }

            }

            // Sort List
            projectedEntities.Sort((x, y) => x.value.CompareTo(y.value));

            //projectedEntities.ForEach(i => Console.Write("{0}\t", i.number));
            //Console.Write("\n");

            // Check which entities might intersect 
            // Broad Phase
            Entity currentEntity = projectedEntities[0].entity;
            int currentEntityNumber = projectedEntities[0].number;

            List<Projection> possibleCollisions = new List<Projection>();

            foreach (var projection in projectedEntities)
            {
                if (!projection.isEnd)
                {
                    // add projection to checklist
                    possibleCollisions.Add(projection);
                    //Console.Write("Added new element to checklist: " + projection.number + "\n");

                    //possibleCollisions.ForEach(i => Console.Write("{0}\t", i.number));
                    //Console.Write("\n");

                }
                else if (projection.isEnd)
                {
                    currentEntityNumber = projection.number;
                    currentEntity = projection.entity;

                    //Console.Write(projection.number + " ended. Removing it from list.\n");


                    // remove the ending member
                    int index = possibleCollisions.FindIndex(x => x.number == currentEntityNumber);
                    if (index != -1)
                    {
                        possibleCollisions.RemoveAt(index);
                    }
                    else
                        continue;

                    // run collisionDetection between the ending member and all remaining members of the checklist
                    foreach (var obstacle in possibleCollisions)
                    {
                        Entity collisionEntity = obstacle.entity;

                        //Console.Write("Checking collision between " + currentEntityNumber + " and " + obstacle.number + "\n");

                        CheckCollision(ref currentEntity, ref collisionEntity);
                    }
                        
                 }
            }

        }

        private void CheckCollision(ref Entity entity0, ref Entity entity1)
        {
            float bounce = 0.99f;
            RigidBody y0;
            RigidBody yfinal;
            Vector4 depth;

            // TODO: Each "if" has three possible cases (RectVSRect, RectVSCircle, CircleVSCircle)
            //       put these into seperate function
            //       implement ellastic collision as own function (for rect vs rect)

            // TODO: Circle vs Rect does not work properly on edges

            if (entity0.Has<RigidBody>() && !entity1.Has<RigidBody>())
            {
                // y0 of entity0 is altered 
                CheckIntersectionAndResolveForSpecificBoundingVolumes(ref entity0, ref entity1, false);
            }
            else if (!entity0.Has<RigidBody>() && entity1.Has<RigidBody>())
            {
                // y0 of entity1 is altered
                CheckIntersectionAndResolveForSpecificBoundingVolumes(ref entity1, ref entity0, false);
            }
            else if (entity0.Has<RigidBody>() && entity1.Has<RigidBody>())
            {
                // y0 of both entities is altered -> ellastic collision
                CheckIntersectionAndResolveForSpecificBoundingVolumes(ref entity0, ref entity1, true);
            }
        }

        private void CheckIntersectionAndResolveForSpecificBoundingVolumes(ref Entity entityWithRigidBody, ref Entity entityWithoutRigidBody, bool bothHaveRigidBodies)
        {
            //Calculate intersection depth
            Vector4 depth;
            RigidBody y0 = entityWithRigidBody.Get<RigidBody>();
            RigidBody yfinal = y0;
            float bounce = 0.99f;
            bool didIntersect = false;
            Vector3 intersectionPoint = new Vector3(0, 0, 0);

            if (entityWithRigidBody.Get<HitBox>().isCircle && !entityWithoutRigidBody.Get<HitBox>().isCircle)
            {
                // check circle vs rect
                if (Intersects(entityWithRigidBody.Get<HitBox>().hitBox, entityWithRigidBody.Get<HitBox>().radius, entityWithoutRigidBody.Get<HitBox>().hitBox, out depth))
                {
                    didIntersect = true;
                    intersectionPoint = new Vector3(depth.X, depth.Y, 0);

                    // resolve intersection (calculate new y0.X)
                    if (bothHaveRigidBodies == false)
                    {
                        // TODO: Rect pos has to be altered, not circle pos
                        yfinal = resolveCircletVsRect(y0, depth, bounce);
                        if (!y0.pinned)  entityWithRigidBody.Set<RigidBody>(yfinal);
                    }
                    // if both are rigid bodies calculate new oldX for both
                    else
                    {
                        RigidBody y0_1 = entityWithoutRigidBody.Get<RigidBody>();
                        Tuple<RigidBody, RigidBody> yfinalTuple = resolveCircleVsRect(y0, y0_1, depth, bounce);

                        if (!y0.pinned) entityWithRigidBody.Set<RigidBody>(yfinalTuple.Item1);
                        if (!y0_1.pinned) entityWithoutRigidBody.Set<RigidBody>(yfinalTuple.Item2);
                    }
                }
            }
            else if (!entityWithRigidBody.Get<HitBox>().isCircle && entityWithoutRigidBody.Get<HitBox>().isCircle)
            {
                // check rect vs circle
                if (Intersects(entityWithoutRigidBody.Get<HitBox>().hitBox, entityWithoutRigidBody.Get<HitBox>().radius, entityWithRigidBody.Get<HitBox>().hitBox, out depth))
                {
                    didIntersect = true;
                    intersectionPoint = new Vector3(depth.X, depth.Y, 0);

                    // resolve intersection (calculate new y0.X)
                    if (bothHaveRigidBodies == false)
                    {
                        // TODO: Rect pos has to be altered, not circle pos
                        yfinal = resolveCircletVsRect(y0, depth, bounce);
                        if (!y0.pinned) entityWithRigidBody.Set<RigidBody>(yfinal);
                    }
                    // if both are rigid bodies calculate new oldX for both
                    else
                    {
                        RigidBody y0_1 = entityWithoutRigidBody.Get<RigidBody>();
                        Tuple<RigidBody, RigidBody> yfinalTuple = resolveCircleVsRect(y0, y0_1, depth, bounce);

                        if (!y0.pinned) entityWithRigidBody.Set<RigidBody>(yfinalTuple.Item1);
                        if (!y0_1.pinned) entityWithoutRigidBody.Set<RigidBody>(yfinalTuple.Item2);
                    }

                    didIntersect = true;
                    intersectionPoint = entityWithRigidBody.Get<RigidBody>().X;
                }
            }
            else if (entityWithRigidBody.Get<HitBox>().isCircle && entityWithoutRigidBody.Get<HitBox>().isCircle)
            {
                // check circle vs circle
                if (Intersects(entityWithRigidBody.Get<HitBox>().hitBox, entityWithRigidBody.Get<HitBox>().radius, 
                    entityWithoutRigidBody.Get<HitBox>().hitBox, entityWithoutRigidBody.Get<HitBox>().radius, out depth))
                {
                    didIntersect = true;
                    intersectionPoint = new Vector3(depth.X, depth.Y, 0);

                    RigidBody y0_1 = entityWithoutRigidBody.Get<RigidBody>();
                   // RigidBody yfinal1;
                    Tuple<RigidBody, RigidBody> yfinalTuple = resolveCircletVsCircle(y0, y0_1, entityWithRigidBody.Get<HitBox>().radius, entityWithoutRigidBody.Get<HitBox>().radius,
                        depth, bounce);

                    if (!y0.pinned) entityWithRigidBody.Set<RigidBody>(yfinalTuple.Item1);
                    if (!y0_1.pinned) entityWithoutRigidBody.Set<RigidBody>(yfinalTuple.Item2);
                }
            }
            else
            {
                //check rect vs rect
                if (Intersects(entityWithRigidBody.Get<HitBox>().hitBox, entityWithoutRigidBody.Get<HitBox>().hitBox, out depth))
                {
                    didIntersect = true;
                    intersectionPoint = new Vector3(depth.X, depth.Y, 0);

                    if (bothHaveRigidBodies == false)
                    {
                        yfinal = resolveRectVsRect(y0, depth, bounce);
                        if (!y0.pinned) entityWithRigidBody.Set<RigidBody>(yfinal);
                    }
                    else
                    {
                        RigidBody y0_1 = entityWithoutRigidBody.Get<RigidBody>();
                        Tuple<RigidBody, RigidBody> yfinalTuple = resolveRectVsRect(y0, y0_1, depth, bounce);

                        if (!y0.pinned) entityWithRigidBody.Set<RigidBody>(yfinalTuple.Item1);
                        if (!y0_1.pinned) entityWithoutRigidBody.Set<RigidBody>(yfinalTuple.Item2);
                    }
                }
            }

            // for visualization
            entityWithRigidBody.Get<RigidBody>().SetIntersectionVariables(didIntersect, intersectionPoint);
        }

        /*public bool CheckCollisions(Scene scene, Entity player, RigidBody y0, out RigidBody yfinal)
        {
            float forceMultiplier = 1 - friction;

            yfinal = y0;
            float bounce = 0.99f;
            //Get a list of objects to test against
            
            foreach (var transform in scene.Root.Children)
            {
                if (transform.Owner.Has<HitBox>() && transform.Owner != player)
                {
                    //Calculate intersection depth
                    Vector4 depth;

                    if (player.Get<HitBox>().isCircle && !transform.Owner.Get<HitBox>().isCircle)
                    {
                        // check circle vs rect
                        if (Intersects(player.Get<HitBox>().hitBox, player.Get<HitBox>().radius, transform.Owner.Get<HitBox>().hitBox, out depth))
                        {
                            yfinal = resolveCircletVsRect(y0, depth, bounce);
                            return true;
                        }
                    }
                    else if (player.Get<HitBox>().isCircle && transform.Owner.Get<HitBox>().isCircle)
                    {
                        // check circle vs circle
                        if (Intersects(player.Get<HitBox>().hitBox, player.Get<HitBox>().radius, transform.Owner.Get<HitBox>().hitBox, transform.Owner.Get<HitBox>().radius, out depth))
                        {
                            RigidBody y1 = transform.Owner.Get<RigidBody>();
                            RigidBody yfinal1;
                            yfinal = resolveCircletVsCircle(y0, y1, player.Get<HitBox>().radius, transform.Owner.Get<HitBox>().radius, depth, bounce, out yfinal1);

                            transform.Owner.Set<RigidBody> (yfinal1);
                            return true;
                        }
                    }
                    else 
                    {
                        //check rect vs rect
                        if (Intersects(player.Get<HitBox>().hitBox, transform.Owner.Get<HitBox>().hitBox, out depth))
                        {
                            yfinal = resolveRectVsRect(y0, depth, bounce);
                            return true;
                        }
                    } 
                }
                else
                    continue;
            }

            return false;
        }*/

        public bool Intersects(Rectangle player, Rectangle block, out Vector4 depth)
        {
            Vector2 depthHorizontal = GetHorizontalIntersectionDepth(player, block);
            Vector2 depthVertical = GetVerticalIntersectionDepth(player, block);


            //depth = new Vector2(GetHorizontalIntersectionDepth(player, block), GetVerticalIntersectionDepth(player, block));

            // depth( new position X, new position Y, hit horizontally, hit vertically) 
            depth = new Vector4(depthHorizontal.X, depthVertical.X, depthHorizontal.Y, depthVertical.Y);


            // Console.WriteLine("result : " + (depth.Y != 0 && depth.X != 0));
            return depth.Y != 0 && depth.X != 0;

        }

        public bool Intersects(Rectangle player, float radiusA , Rectangle rect, out Vector4 depth)
        {
            depth = new Vector4(0, 0, 0, 0);

            float halfHeight = player.Height / 2.0f;
            float halfWidth = player.Width / 2.0f;

            // Calculate centers.
            Vector2 centerA = new Vector2(player.Left + halfWidth, player.Top + halfHeight);

            float testX = centerA.X;
            float testY = centerA.Y;
            Vector2 newPos = new Vector2(0, 0);

            // which edge is closest?
            if (centerA.X < rect.Left)
            {
                testX = rect.Left;      // test left edge
                newPos.X = testX - radiusA;
            }
            else if (centerA.X > rect.Left + rect.Width)
            {
                testX = rect.Left + rect.Width;   // right edge
                newPos.X = testX + radiusA;
            }

            if (centerA.Y < rect.Top)
            {
                testY = rect.Top;      // top edge
                newPos.Y = testY - radiusA;
            }
            else if (centerA.Y > rect.Top + rect.Height)
            {
                testY = rect.Top + rect.Height;   // bottom edge
                newPos.Y = testY + radiusA;
            }

            // get distance from closest edges
            float distX = centerA.X - testX;
            float distY = centerA.Y - testY;
            float distance = (float)Math.Sqrt((distX * distX) + (distY * distY));

            // if the distance is less than the radius, collision!
            if (distance < radiusA)
            {
                depth = new Vector4(newPos.X, newPos.Y, distX, distY);
                return true;
            }

            return false;
        }

        public bool Intersects(Rectangle player, float radiusA, Rectangle obstacle, float radiusB, out Vector4 depth)
        {
            depth = new Vector4(0, 0, 0, 0);

            float halfHeightA = player.Height / 2.0f;
            float halfWidthA = player.Width / 2.0f;
            float halfHeightB = obstacle.Height / 2.0f;
            float halfWidthB = obstacle.Width / 2.0f;

            // Calculate centers.
            Vector2 centerA = new Vector2(player.Left + halfWidthA, player.Top + halfHeightA);
            Vector2 centerB = new Vector2(obstacle.Left + halfWidthB, obstacle.Top + halfHeightB);

            // get distance between the circle's centers
            // use the Pythagorean Theorem to compute the distance
            float distX = centerA.X - centerB.X;
            float distY = centerA.Y - centerB.Y;
            float distance = (float)Math.Sqrt((distX * distX) + (distY * distY));

            // if the distance is less than the sum of the circle's
            // radii, the circles are touching!
            if (distance <= radiusA + radiusB)
            {
                return true;
            }
            return false;
        }
   

        public static Vector2 GetVerticalIntersectionDepth(Rectangle rectA, Rectangle rectB)
        {
            // Calculate half sizes.
            float halfHeightA = rectA.Height / 2.0f;
            float halfHeightB = rectB.Height / 2.0f;

            // Calculate centers.
            float centerA = rectA.Top + halfHeightA; // + or - ?
            float centerB = rectB.Top + halfHeightB;

            // Calculate bottoms.
            float bottomA = rectA.Top + rectA.Height;
            float bottomB = rectB.Top + rectB.Height;

            // Calculate current and minimum-non-intersecting distances between centers.
            float distanceY = centerA - centerB;
            float minDistanceY = halfHeightA + halfHeightB;


            if (rectA.Top < rectB.Top && bottomA > rectB.Top)
            {
                //Console.WriteLine("player hit rectB on top \n");
                float distanceToTop = bottomA - rectB.Top;
                return new Vector2 (rectB.Top - halfHeightA, distanceToTop);
            }
            else if (rectA.Top < bottomB && bottomA > bottomB)
            {
                //Console.WriteLine("player hit rectB on bottom \n");
                float distanceToBottom = rectA.Top - bottomB;
                return new Vector2(bottomB + halfHeightA, distanceToBottom);
            }


            if (Math.Abs(distanceY) >= minDistanceY)
                return new Vector2(0f, 0f);

            // Calculate and return intersection depths.
            // (condition) ? [true path] : [false path]
            return new Vector2(distanceY > 0 ? minDistanceY - distanceY : -minDistanceY - distanceY, 0f);
        }

        public static Vector2 GetHorizontalIntersectionDepth(Rectangle rectA, Rectangle rectB)
        {
            // Calculate half sizes.
            float halfWidthA = rectA.Width / 2.0f;
            float halfWidthB = rectB.Width / 2.0f;

            // Calculate centers.
            float centerA = rectA.Left + halfWidthA;
            float centerB = rectB.Left + halfWidthB;

            // Calculate rights.
            float rightA = rectA.Left + rectA.Width;
            float rightB = rectB.Left + rectB.Width;

            // Calculate current and minimum-non-intersecting distances between centers.
            float distanceX = centerA - centerB;
            float minDistanceX = halfWidthA + halfWidthB;

            
            if (rectA.Left < rectB.Left && rightA > rectB.Left)
            {
                //Console.WriteLine("player hit rectB on the left \n");
                float distanceToLeft = rightA - rectB.Left;
                return new Vector2(rectB.Left - halfWidthA, distanceToLeft);
            }
            else if (rectA.Left < rightB && rightA > rightB)
            {
                //Console.WriteLine("player hit rectB on the right \n");
                float distanceToRight = rectB.Left - rightB;
                return new Vector2(rightB + halfWidthA, distanceToRight);
            }

           
            // If we are not intersecting at all, return (0, 0).
            if (Math.Abs(distanceX) >= minDistanceX)
                return new Vector2(0f, 0f);

            // Calculate and return intersection depths.
            return new Vector2(distanceX > 0 ? minDistanceX - distanceX : -minDistanceX - distanceX, 0f); 
        }

        public void UpdateHitBox(Entity Object)
        {
            Vector3 localPosition = Object.Get<Transform>().LocalMatrix.GetTranslation();

            float xLength = Object.Get<HitBox>().xLength; //
            float yLength = Object.Get<HitBox>().yLength; //

            Object.Get<HitBox>().min = new Vector3(localPosition.X - xLength, localPosition.Y + yLength, 0); //
            Object.Get<HitBox>().max = new Vector3(localPosition.X + xLength, localPosition.Y - yLength, 0); //

            float x = localPosition.X - (Object.Get<HitBox>().width / 2);
            float y = localPosition.Y - (Object.Get<HitBox>().height / 2);

            Object.Get<HitBox>().hitBox = new Rectangle((int)x, (int)y, (int)Object.Get<HitBox>().width, (int)Object.Get<HitBox>().height);
        }

        public float calcStraightLineEquation(Vector3 point1, Vector3 point2, float val, float currentX,  string axis)
        {
            if((point2.Y - point1.Y) == 0 || (point2.X - point1.X) == 0)
            {
                return currentX;
            }

            float k = (point2.Y - point1.Y) / (point2.X - point1.X);
            float d = point1.Y - (k * point1.X);

            if(axis == "x")
            {
                return (val - d) / k;
            }
            else if (axis == "y")
            {
                return k * val + d;
            }

            return currentX;
        }

        private Tuple<RigidBody, RigidBody> resolveRectVsRect(RigidBody y0_0, RigidBody y0_1, Vector4 depth, float bounce)
        {
            RigidBody yfinal_0 = y0_0;
            RigidBody yfinal_1 = y0_1;
            Tuple<RigidBody, RigidBody> yfinalTuple =  new Tuple<RigidBody, RigidBody>(y0_0, y0_1);

            float xAxisTimeToCollide = y0_0.v.X != 0 ? Math.Abs(depth.Z / y0_0.v.X) : 0;
            float yAxisTimeToCollide = y0_0.v.Y != 0 ? Math.Abs(depth.W / y0_0.v.Y) : 0;

            yfinal_0.X.X = depth.X;
            yfinal_0.X.Y = depth.Y;

            Tuple<Vector3, Vector3> finalVelocityTuple = ComputeElasticCollision(y0_0, y0_1);

            // calculate impulse
            Vector3 dv_0 = y0_0.v - finalVelocityTuple.Item1;
            Vector3 dv_1 = y0_1.v - finalVelocityTuple.Item2;
            yfinal_0.J = dv_0 * y0_0.Mass;
            yfinal_1.J = dv_1 * y0_1.Mass;
            yfinal_0.q.W *= -0.9f;
            yfinal_1.q.W *= -0.9f;

            yfinal_0.oldX = -(finalVelocityTuple.Item1 - yfinal_0.X);
            yfinal_1.oldX = -(finalVelocityTuple.Item2 - yfinal_1.X);

            yfinalTuple = new Tuple<RigidBody, RigidBody>(yfinal_0, yfinal_1);

            return yfinalTuple;
        }

        private RigidBody resolveRectVsRect(RigidBody y0, Vector4 depth, float bounce)
        {
            RigidBody yfinal = y0; 
            float xAxisTimeToCollide = y0.v.X != 0 ? Math.Abs(depth.Z / y0.v.X) : 0;
            float yAxisTimeToCollide = y0.v.Y != 0 ? Math.Abs(depth.W / y0.v.Y) : 0;


            if ((depth.Z != 0f && depth.W == 0f))
            {
                // if the correct Y coordinate is calculated, it ist possible, that it exceeds the previous
                // (oldX.Y) coordinate, thereby the bounce direction is reversed

                //yfinal.X.Y = calcStraightLineEquation(yfinal.oldX, yfinal.X, depth.X, y0.X.Y, "y");

                yfinal.X.X = depth.X;
                //yfinal.oldX.X = (yfinal.X.X + y0.v.X) * bounce;

                float pointOfOrigin = (yfinal.X.X + y0.v.X) * bounce;

                if (pointOfOrigin <= yfinal.X.X)
                {
                    yfinal.oldX.X = yfinal.X.X;
                }
                else
                {
                    yfinal.oldX.X = pointOfOrigin;
                }
                yfinal.q.W *= -0.9f;
            }
            else if (depth.W != 0f && depth.Z == 0f)
            {
                // if the correct X coordinate is calculated, it ist possible, that it exceeds the previous
                // (oldX.X) coordinate, thereby the bounce direction is reversed

                //yfinal.X.X = calcStraightLineEquation(yfinal.oldX, yfinal.X, depth.Y, y0.X.X, "x");

                yfinal.X.Y = depth.Y;
                //yfinal.oldX.Y = (yfinal.X.Y + y0.v.Y) * bounce;
                float pointOfOrigin = (yfinal.X.Y + y0.v.Y) * bounce;

                if (pointOfOrigin <= yfinal.X.Y)
                {
                    yfinal.oldX.Y = yfinal.X.Y;
                }
                else
                {
                    yfinal.oldX.Y = pointOfOrigin;
                }

            }
            // corner cases 
            else if (xAxisTimeToCollide < yAxisTimeToCollide)
            {
                //yfinal.X.X = depth.X;
                //yfinal.oldX.X = (yfinal.X.X + y0.v.X) * bounce;

                yfinal.X.X = depth.X;
                //yfinal.oldX.X = (yfinal.X.X + y0.v.X) * bounce;

                float pointOfOrigin = (yfinal.X.X + y0.v.X) * bounce;

                if (pointOfOrigin <= yfinal.X.X)
                {
                    yfinal.oldX.X = yfinal.X.X;
                }
                else
                {
                    yfinal.oldX.X = pointOfOrigin;
                }
                yfinal.q.W *= -0.9f;
            }
            else
            {
                //yfinal.X.Y = depth.Y;
                //yfinal.oldX.Y = (yfinal.X.Y + y0.v.Y) * bounce;

                yfinal.X.Y = depth.Y;
                //yfinal.oldX.Y = (yfinal.X.Y + y0.v.Y) * bounce;
                float pointOfOrigin = (yfinal.X.Y + y0.v.Y) * bounce;

                if (pointOfOrigin <= yfinal.X.Y)
                {
                    yfinal.oldX.Y = yfinal.X.Y;
                }
                else
                {
                    yfinal.oldX.Y = pointOfOrigin;
                }
            }

            return yfinal;
        }

        private Tuple<RigidBody, RigidBody> resolveCircleVsRect(RigidBody y0_0, RigidBody y0_1, Vector4 depth, float bounce)
        {
            RigidBody yfinal_0 = y0_0;
            RigidBody yfinal_1 = y0_1;
            Tuple<RigidBody, RigidBody> yfinalTuple = new Tuple<RigidBody, RigidBody>(y0_0, y0_1);

            float xAxisTimeToCollide = y0_0.v.X != 0 ? Math.Abs(depth.Z / y0_0.v.X) : 0;
            float yAxisTimeToCollide = y0_0.v.Y != 0 ? Math.Abs(depth.W / y0_0.v.Y) : 0;

            yfinal_0.X.X = depth.X;
            yfinal_0.X.Y = depth.Y;

            Tuple<Vector3, Vector3> finalVelocityTuple = ComputeElasticCollision(y0_0, y0_1);

            // calculate impulse
            Vector3 dv_0 = y0_0.v - finalVelocityTuple.Item1;
            Vector3 dv_1 = y0_1.v - finalVelocityTuple.Item2;
            yfinal_0.J = dv_0 * y0_0.Mass;
            yfinal_1.J = dv_1 * y0_1.Mass;
            yfinal_0.q.W *= -0.99f;
            yfinal_1.q.W *= -0.99f;

            yfinal_0.oldX = -(finalVelocityTuple.Item1 - yfinal_0.X);
            yfinal_1.oldX = -(finalVelocityTuple.Item2 - yfinal_1.X);

            yfinalTuple = new Tuple<RigidBody, RigidBody>(yfinal_0, yfinal_1);

            return yfinalTuple;
        }

        private RigidBody resolveCircletVsRect(RigidBody y0, Vector4 depth, float bounce)
        {
            RigidBody yfinal = y0;
            float pointOfOrigin = 0f;

            float xAxisTimeToCollide = y0.v.X != 0 ? Math.Abs(depth.Z / y0.v.X) : 0;
            float yAxisTimeToCollide = y0.v.Y != 0 ? Math.Abs(depth.W / y0.v.Y) : 0;


            //yfinal.X = new Vector3(depth.X, depth.Y, y0.X.Z);

            if(depth.Z != 0f && depth.W == 0f)
            {
                yfinal.X.X = depth.X;
                pointOfOrigin = (yfinal.X.X + y0.v.X) * bounce;
                yfinal.oldX.X = pointOfOrigin;
                yfinal.q.W *= -0.99f;
            }
            else if (depth.Z == 0f && depth.W != 0f)
            {
                yfinal.X.Y = depth.Y;
                pointOfOrigin = (yfinal.X.Y + y0.v.Y);// * bounce;
                yfinal.oldX.Y = pointOfOrigin;
                yfinal.q.W *= 0.99f;
            }
            else if (xAxisTimeToCollide < yAxisTimeToCollide)
            {
                yfinal.X.X = depth.X;
                pointOfOrigin = (yfinal.X.X + y0.v.X) * bounce;
                yfinal.oldX.X = pointOfOrigin;
                yfinal.q.W *= -0.99f;
            }
            else //(xAxisTimeToCollide >= yAxisTimeToCollide)
            {
                yfinal.X.Y = depth.Y;
                pointOfOrigin = (yfinal.X.Y + y0.v.Y) * bounce;
                yfinal.oldX.Y = pointOfOrigin;
                yfinal.q.W *= 0.99f;
            }
            
            return yfinal;
        }

        private Tuple<RigidBody, RigidBody> resolveCircletVsCircle(RigidBody y0_0, RigidBody y0_1, float radiusA, float radiusB, Vector4 depth, float bounce)
        {
            RigidBody yfinal_0 = y0_0;
            RigidBody yfinal_1 = y0_1;

            Tuple<RigidBody, RigidBody> yfinalTuple = new Tuple<RigidBody, RigidBody>(y0_0, y0_1);

            // resolve collision
            float dx = y0_1.X.X - y0_0.X.X;
            float dy = y0_1.X.Y - y0_0.X.Y;
            float distance = (float)Math.Sqrt(dx * dx + dy * dy);
            float difference = (radiusA + radiusB + 1) - distance;
            float percent = difference / distance / 2;

            if (percent != 0)
            {
                float offsetX = dx * percent;
                float offsetY = dy * percent;

                yfinal_0.X.X -= offsetX;
                yfinal_0.X.Y -= offsetY;
                yfinal_1.X.X += offsetX;
                yfinal_1.X.Y += offsetY;
            }

            Tuple<Vector3, Vector3> finalVelocityTuple = ComputeElasticCollision(y0_0, y0_1);

            // calculate impulse

            // TODO: create proper function for this part 
            Vector3 dv_0 = y0_0.v - finalVelocityTuple.Item1;
            Vector3 dv_1 = y0_1.v - finalVelocityTuple.Item2;
            yfinal_0.J = dv_0 * y0_0.Mass;
            yfinal_1.J = dv_1 * y0_1.Mass;
            yfinal_0.q.W *= -0.99f;
            yfinal_1.q.W *= -0.99f;

            yfinal_0.oldX = -(finalVelocityTuple.Item1 - yfinal_0.X);
            yfinal_1.oldX = -(finalVelocityTuple.Item2 - yfinal_1.X);

            yfinalTuple = new Tuple<RigidBody, RigidBody>(yfinal_0, yfinal_1);

            return yfinalTuple;
        }

        public Tuple<Vector3, Vector3> ComputeElasticCollision(RigidBody y0_0, RigidBody y0_1)
        {
            Tuple<Vector3, Vector3> finalVelocityTuple = new Tuple<Vector3, Vector3>(y0_0.v, y0_1.v);

            // calculate impulse / new velocities
            Vector3 X0X1 = y0_0.X - y0_1.X;
            Vector3 X1X0 = y0_1.X - y0_0.X;
            float normX0X1 = (float)Math.Sqrt((X0X1.X * X0X1.X) + (X0X1.Y * X0X1.Y));
            float normX1X0 = (float)Math.Sqrt((X1X0.X * X1X0.X) + (X1X0.Y * X1X0.Y));

            Vector3 dv_0 = ((2 * y0_0.Mass) / y0_0.Mass + y0_1.Mass) * ((Vector3.Dot(y0_0.v - y0_1.v, y0_0.X - y0_1.X)) / (normX0X1 * normX0X1)) * (y0_0.X - y0_1.X);
            Vector3 dv_1 = ((2 * y0_1.Mass) / y0_1.Mass + y0_0.Mass) * ((Vector3.Dot(y0_1.v - y0_0.v, y0_1.X - y0_0.X)) / (normX1X0 * normX1X0)) * (y0_1.X - y0_0.X);

            Vector3 finalVelocity_0 = y0_0.v - (dv_0 * 0.1f);
            Vector3 finalVelocity_1 = y0_1.v - (dv_1 * 0.1f);

            finalVelocityTuple = new Tuple<Vector3, Vector3>(finalVelocity_0, finalVelocity_1);
            return finalVelocityTuple;
        }
        
        public static Collision? TestCollision(Vector2 before, Vector2 after, Rectangle rect)
        {
            var trajectory = Tuple.Create(before, after);

            // a---d
            // |   |
            // b---c
            var a = new Vector2(rect.X, rect.Y);
            var b = new Vector2(rect.X, rect.Y + rect.Height);
            var c = new Vector2(rect.X + rect.Width, rect.Y + rect.Height);
            var d = new Vector2(rect.X + rect.Width, rect.Y);

            if (before.X < rect.X)
            {
                //    a--
                // -> |
                //    b--
                var pointOfCollision = IntersectLines(trajectory, Tuple.Create(a, b));
                if (pointOfCollision != null)
                {
                    return new Collision()
                    {
                        Point = ((Vector2)pointOfCollision),
                        Normal = new Vector2(-1, 0)
                    };
                }
                if (before.Y > rect.Y)
                {                    
                    //   |   |
                    //   b---c
                    //  ---^                      
                    pointOfCollision = IntersectLines(trajectory, Tuple.Create(c, b));
                    if (pointOfCollision != null)
                    {
                        return new Collision()
                        {
                            Point = ((Vector2)pointOfCollision),
                            Normal = new Vector2(0, 1)
                        };
                    }
                }
                else
                {
                    //  ---v 
                    //   a---d
                    //   |   |                                       
                    pointOfCollision = IntersectLines(trajectory, Tuple.Create(d, a));
                    if (pointOfCollision != null)
                    {
                        return new Collision()
                        {
                            Point = ((Vector2)pointOfCollision),
                            Normal = new Vector2(0, -1)
                        };
                    }
                }
            }
            else if (before.X > rect.X + rect.Width)
            {
                // ---d
                //    | <--
                // ---c
                var pointOfCollision = IntersectLines(trajectory, Tuple.Create(c, d));
                if (pointOfCollision != null)
                {
                    return new Collision()
                    {
                        Point = ((Vector2)pointOfCollision),
                        Normal = new Vector2(1, 0)
                    };
                }
                if (before.Y > rect.Y)
                {                    
                    // |   |
                    // b---c
                    //   ^---                      
                    pointOfCollision = IntersectLines(trajectory, Tuple.Create(b, c));
                    if (pointOfCollision != null)
                    {
                        return new Collision()
                        {
                            Point = ((Vector2)pointOfCollision),
                            Normal = new Vector2(0, 1)
                        };
                    }
                }
                else
                {
                    //   v--- 
                    // a---d
                    // |   |                    
                    pointOfCollision = IntersectLines(trajectory, Tuple.Create(d, a));
                    if (pointOfCollision != null)
                    {
                        return new Collision()
                        {
                            Point = ((Vector2)pointOfCollision),
                            Normal = new Vector2(0, -1)
                        };
                    }
                }
            }
            else
            {
                if (before.Y > rect.Y)
                {                    
                    // |   |
                    // b---c 
                    //   ^
                    //   |
                    var pointOfCollision = IntersectLines(trajectory, Tuple.Create(c, b));
                    if (pointOfCollision != null)
                    {
                        return new Collision()
                        {
                            Point = ((Vector2)pointOfCollision),
                            Normal = new Vector2(0, 1)
                        };
                    }
                }
                else
                {
                    //   |
                    //   V
                    // a---d
                    // |   |                            
                    var pointOfCollision = IntersectLines(trajectory, Tuple.Create(d, a));
                    if (pointOfCollision != null)
                    {
                        return new Collision()
                        {
                            Point = ((Vector2)pointOfCollision),
                            Normal = new Vector2(0, -1)
                        };
                    }
                }
            }

            return null;
        }

        /**
         * https://en.wikipedia.org/wiki/Intersection_(Euclidean_geometry)#Two_line_segments
         */
        public static Vector2? IntersectLines(Tuple<Vector2, Vector2> lineA, Tuple<Vector2, Vector2> lineB)
        {
            float x1 = lineA.Item1.X;
            float x2 = lineA.Item2.X;
            float x3 = lineB.Item1.X;
            float x4 = lineB.Item2.X;

            float y1 = lineA.Item1.Y;
            float y2 = lineA.Item2.Y;
            float y3 = lineB.Item1.Y;
            float y4 = lineB.Item2.Y;

            float divisor = (x2 - x1) * (y3 - y4) - (y2 - y1) * (x3 - x4);
            if (divisor == 0)
            {
                // lines are parallel
                return null;
            }

            // find parameters
            float p1 = ((x3 - x1) * (y3 - y4) - (y3 - y1) * (x3 - x4)) / divisor;
            float p2 = ((y3 - y1) * (x2 - x1) - (x3 - x1) * (y2 - y1)) / divisor;

            // required: 0 <= p1,p2 <= 1
            if (p1 < 0 || p1 > 1 || p2 < 0 || p2 > 1)
            { 
                return null;  
            }

            // either parameter can be used to find solution
            return (lineA.Item1 + p1 * (lineA.Item2 - lineA.Item1));
        }
        
    }
}
