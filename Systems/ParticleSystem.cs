﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using DefaultEcs;

namespace MercurySoldier.Systems
{
    using Components;
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using System.Linq;

    class ParticleSystem
    {

        public enum IntegrationMethod
        {
            Euler,
            RungeKutta
        }

        public const float maxVelocity = 1000f;
        public IntegrationMethod MethodOfIntegration = IntegrationMethod.RungeKutta;

        // player controls
        private Vector2 controlForce = Vector2.Zero;
        private float controlledParticleSpeed = 500f;
        private ParticleCollection playerControlledParticles;

        private List<Vector2> globalForces = new List<Vector2>();
        private List<Tuple<Rectangle, Vector2>> localForces = new List<Tuple<Rectangle, Vector2>>();
        private float dragCoefficient = .1f;

        public ParticleSystem()
        {   
            
        }

        public void Update(Scene scene, float deltaTime)
        {
            var particleCollection = scene.World.GetEntities()
                .With<ParticleCollection>()
                .AsEnumerable()
                .Select(e => e.Get<ParticleCollection>());
                        
            foreach (var pCollection in particleCollection)
            {
                Update(scene, deltaTime, pCollection);
            }
        }

        public void AddGlobalForce(Vector2 force)
        {
            globalForces.Add(force);
        }

        public void AddLocalForce(Rectangle area, Vector2 force)
        {            
            localForces.Add(Tuple.Create(area, force));
        }
        
        public Vector2 GetLocalForceAt(Vector2 position)
        {
            var accumulatedForce = Vector2.Zero;
            foreach (var force in localForces)
            {
                if (force.Item1.Contains(position))
                {
                    accumulatedForce += force.Item2;
                }                
            }
            return accumulatedForce;
        }

        protected void Update(Scene scene, float deltaTime, ParticleCollection particleCollection)
        {
            var positionsBefore = particleCollection.Particles.Select(p => p.Pos).ToArray();
            var currentState = particleCollection.GetState();
            var extraForce = (particleCollection == playerControlledParticles) ? controlForce : Vector2.Zero;

            Func<float[], float, ODESolver.EvalFunction, float[]> IntegrationFunction = MethodOfIntegration switch
            {
                IntegrationMethod.RungeKutta => ODESolver.RungeKutta,
                _ => ODESolver.Euler
            };

            var newStateBeforeCollision =
                IntegrationFunction(currentState, deltaTime, (_, state) =>  
                    particleCollection.Derivative(state, particles => 
                        ApplyForces(particles, extraForce, particleCollection.StickTogether)
                    )
                );


            particleCollection.ApplyState(newStateBeforeCollision);
            var positionsAfter = particleCollection.Particles.Select(p => p.Pos).ToArray();

            float friction = .4f;
            for (int i = 0; i < positionsBefore.Length; i++)
            {
                foreach (var hitbox in scene.World.GetEntities().With<HitBox>().AsEnumerable().Select(e => e.Get<HitBox>()))
                {

                    // TODO: cirlce collision
                    var possibleCollision = CollisionDetectionSystem.TestCollision(positionsBefore[i], positionsAfter[i], hitbox.hitBox);
                    if (possibleCollision == null)
                        continue;

                    var collision = possibleCollision.Value;
                    var reflection = Vector2.Reflect(particleCollection.Particles[i].Vel, collision.Normal);

                    // clamp low velocities to prevent jitter
                    if (reflection.Length() < 0.001f)
                    {
                        particleCollection.Particles[i].Vel = Vector2.Zero;
                        particleCollection.Particles[i].Pos = collision.Point;
                        continue;
                    }
                                        
                    particleCollection.Particles[i].Vel = reflection * (1 - friction);
                    reflection.Normalize();
                    var newPos = collision.Point + reflection * (positionsAfter[i] - collision.Point).Length() * (1 - friction);
                    particleCollection.Particles[i].Pos = newPos;
                }

                float magnitudeVelocity = particleCollection.Particles[i].Vel.Length();
                particleCollection.Particles[i].Vel.Normalize();
                particleCollection.Particles[i].Vel *= Math.Clamp(magnitudeVelocity, -maxVelocity, maxVelocity);
            }            
            // Console.WriteLine(string.Join(',', currentState.Skip(4).Take(4)));
        }

        private void ApplyForces(Particle[] particles, Vector2 extraForce, bool stickTogether = true)
        {
            for (int particleIndex = 0; particleIndex < particles.Length; particleIndex++)
            {
                particles[particleIndex].FAcc += extraForce;

                foreach (var force in globalForces)
                {
                    particles[particleIndex].FAcc += force;
                }

                foreach(var force in localForces)
                {
                    var pos = particles[particleIndex].Pos;
                    if(force.Item1.Contains(pos))
                    {                        
                        particles[particleIndex].FAcc += force.Item2;
                    }
                }

                // linear drag
                particles[particleIndex].FAcc -= dragCoefficient * particles[particleIndex].Vel;
            }

            // accelerate towards center of "gravity"
            if (stickTogether)
            {
                var rnd = new Random();
                var averagePosition = particles.Select(p => p.Pos).Aggregate((carry, pos) => carry + pos) / particles.Length;
                for (int particleIndex = 0; particleIndex < particles.Length; particleIndex++)
                {
                    var towardsCenter = averagePosition - particles[particleIndex].Pos;
                    var distanceTowardsCenter = towardsCenter.Length();
                    towardsCenter.Normalize();                    
                    var accelerationTowardsCenter = towardsCenter * MathF.Exp(distanceTowardsCenter / 20);
                    particles[particleIndex].FAcc += accelerationTowardsCenter;
                }
            }
        }

        public void SetPlayerParticles(ParticleCollection playerParticles)
        {
            playerControlledParticles = playerParticles;
        }

        public void ControlParticles(Vector2 direction)
        {
            if (direction == Vector2.Zero)
            {
                controlForce = Vector2.Zero;
                return;
            }
            direction.Normalize();
            controlForce = direction * controlledParticleSpeed;
        }
    }
}
