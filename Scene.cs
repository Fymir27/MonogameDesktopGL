﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DefaultEcs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace MercurySoldier
{
    using Components;
    using System.Linq;

    class Scene
    {
        public readonly World World = new World();
        public Transform Root { get; private set; } = new Transform();

        public readonly Color BlueprintColor = new Color(100, 150, 255, 100); // transparent blue

        // the object to be placed (following the mouse)
        private Entity? currentlyActiveBlueprintEntity;
        private static Dictionary<string, Blueprint> availableBluePrints = new Dictionary<string, Blueprint>();
        private static List<string> bluePrintNames = new List<string>();
        private int currentBlueprintIndex = 0;

        public Scene()
        {
            World.SubscribeEntityDisposed(RemoveFromHierarchy);
        }

        public static Scene FromJson(string jsonString)
        {            
            var loadedScene = new Scene();

            var identity = Matrix.Identity;
            identity.Translation = new Vector3(100f, 100f, 0f);
            Console.WriteLine(JsonConvert.SerializeObject(identity));

            var entityList = JArray.Parse(jsonString);
            
            foreach (var jToken in entityList)
            {
                var jObject = jToken as JObject;
                var localMatrix = jObject["localMatrix"].ToObject<Matrix>();
                var blueprintName = jObject["blueprint"].ToString();                
                if (!availableBluePrints.ContainsKey(blueprintName))
                {
                    Console.WriteLine("[Blueprint] Trying to instantiate unkown blueprint: " + blueprintName);
                    continue;
                }                
                var blueprint = availableBluePrints[blueprintName];
                var entity = loadedScene.CreateEntity(localMatrix, loadedScene.Root);
                blueprint.InitEntity(ref entity);
            }

            return loadedScene;
        }

        public string ToJson()
        {
            return new JArray { Root.Children.Select(ToJObject) }.ToString();
        }

        private JObject ToJObject(Transform transform)
        {
            var name = "UNKOWN";

            if (transform.Owner.Has<Blueprint>())
            {
                name = transform.Owner.Get<Blueprint>().Name;                
            }            

            return new JObject
            {
                { "localMatrix", JObject.FromObject(transform.LocalMatrix) },
                { "blueprint", name }
            };
        }

        public void AddToHierarchy(Transform transform)
        {
            AddToHierarchy(transform, Root);
        }

        public void AddToHierarchy(Transform transform, Transform parent)
        {
            // init parent/child relationship
            transform.Parent = parent;
            if (parent.Children is null)
            {
                parent.Children = new List<Transform>();
            }
            parent.Children.Add(transform);
        }

        public void RemoveFromHierarchy(in Entity entity)
        {
            var transform = entity.Get<Transform>();

            if (transform == Root)
            {
                throw new Exception("Cannot remove root transform from hierarchy!");
            }

            if (transform.Children != null)
            {
                foreach (var childTransform in transform.Children)
                {
                    childTransform.Owner.Dispose();
                }
            }

            if (transform.Parent.Children != null)
            {
                transform.Parent.Children.Remove(transform);
            }
        }

        public Entity CreateEntity(Vector3 worldPosition)
        {
            return CreateEntity(worldPosition, Root);
        }

        public Entity CreateEntity(Vector3 position, Entity parent, bool localPosition = true)
        {
            var parentTransform = parent.Get<Transform>();
            return CreateEntity(position, parentTransform, localPosition);
        }

        public Entity CreateEntity(Vector3 position, Transform parentTransform, bool localPosition = true)
        {
            var localMatrix = Matrix.Identity;
            if (localPosition)
            {
                localMatrix.SetTranslation(position);
            }
            else
            {
                localMatrix.SetTranslation(position - parentTransform.GetGlobalMatrix().GetTranslation());
            }

            return CreateEntity(localMatrix, parentTransform);
        }

        public Entity CreateEntity(Matrix localMatrix, Transform parentTransform)
        {
            // create entity and add transform to it
            var entity = World.CreateEntity();
            var entityTransform = new Transform();
            entity.Set(entityTransform);
            entityTransform.Owner = entity;

            AddToHierarchy(entityTransform, parentTransform);           
            entityTransform.LocalMatrix = localMatrix;       
            return entity;
        }

        public void UpdateCurrentBlueprintPosition(Vector3 worldPosition, float angle)
        {
            if (currentlyActiveBlueprintEntity == null) 
            {
                return;
            }

            var bluePrintTransform = currentlyActiveBlueprintEntity.Value.Get<Transform>();
            var pos2D = new Vector2(worldPosition.X, worldPosition.Y).ToPoint();
            pos2D.X -= pos2D.X % 16;
            pos2D.Y -= pos2D.Y % 16;
            bluePrintTransform.LocalMatrix.SetTranslation(new Vector3(pos2D.X, pos2D.Y, 0));
            bluePrintTransform.LocalMatrix.SetRotation(Quaternion.CreateFromAxisAngle(Vector3.Forward, angle));
            //Console.WriteLine(bluePrintTransform.LocalMatrix.Translation);
        }

        public void SetEnabledRecursively(Entity entity, bool enable = true)
        {
            if (enable)
            {
                entity.Enable<SpriteRenderer>();
                entity.Enable();
            }
            else
            {
                entity.Disable<SpriteRenderer>();
                entity.Disable();
            }
                

            if (!entity.Has<Transform>())
                return;

            var children = entity.Get<Transform>().Children ?? null;

            if (children == null)
                return;

            foreach (var transform in children)
            {
                SetEnabledRecursively(transform.Owner, enable);
            }
        }

        public static void AddBlueprint(Blueprint blueprint)
        {
            if (availableBluePrints.ContainsKey(blueprint.Name))
            {
                throw new Exception("Blueprint with that name already exists: " + blueprint.Name);
            }

            bluePrintNames.Add(blueprint.Name);
            availableBluePrints.Add(blueprint.Name, blueprint);
        }

        public static void RemoveBlueprint(string name, Blueprint blueprint)
        {
            if (!availableBluePrints.ContainsKey(name))
            {
                throw new Exception("Blueprint with that name doesn't exists: " + name);
            }

            bluePrintNames.Remove(name);
            availableBluePrints.Remove(name);
            // TODO: check blueprint index?
        }

        public static string[] GetAvailableBlueprints()
        {
            return bluePrintNames.ToArray();
        }

        public void StopBlueprintMode()
        {
            DestroyCurrentBlueprint();
        }

        public void StartBlueprintMode()
        {
            if (bluePrintNames.Count == 0)
            {
                throw new Exception("No blueprints available!");
            }
                        
            StartBlueprintMode(bluePrintNames[currentBlueprintIndex]);
        }

        public void StartBlueprintMode(string blueprintName)
        {
            if (!availableBluePrints.ContainsKey(blueprintName))
            {
                throw new Exception("Unknown blueprint: " + blueprintName);
            }

            //Console.WriteLine(currentlyActiveBlueprintEntity);
            DestroyCurrentBlueprint();
            var blueprint = availableBluePrints[blueprintName];
            currentBlueprintIndex = bluePrintNames.IndexOf(blueprintName);
            currentlyActiveBlueprintEntity = CreateEntity(Vector3.Zero);
            currentlyActiveBlueprintEntity.Value.Set(new SpriteRenderer(blueprint.Texture, blueprint.SpriteSize, BlueprintColor, blueprint.DrawMode));
        }

        public void ChangeBlueprint(bool next = true)
        {
            var indexDelta = next ? +1 : -1;
            var nextIndex = (currentBlueprintIndex + indexDelta) % bluePrintNames.Count;
            if (nextIndex < 0)
            {
                nextIndex = bluePrintNames.Count + nextIndex;
            }
            StartBlueprintMode(bluePrintNames[nextIndex]);
        }        

        private void DestroyCurrentBlueprint()
        {
            if (currentlyActiveBlueprintEntity != null)
            {
                currentlyActiveBlueprintEntity.Value.Dispose();
                currentlyActiveBlueprintEntity = null;
            }
        }

        internal void PlaceBlueprint()
        {
            if (currentlyActiveBlueprintEntity == null)
            {
                return;
            }

            var currentBlueprintName = bluePrintNames[currentBlueprintIndex];
            var currentBlueprint = availableBluePrints[currentBlueprintName];
            var entity = currentlyActiveBlueprintEntity.Value;
            currentBlueprint.InitEntity(ref entity);
            
            currentlyActiveBlueprintEntity = null;
            Console.WriteLine("[Blueprint] Placed " + currentBlueprintName);
            StopBlueprintMode();
        }
    }
}
