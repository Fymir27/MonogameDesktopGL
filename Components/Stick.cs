﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using DefaultEcs;
using System;
using MercurySoldier.Systems;

using CenterSpace.NMath.Core;

using DefaultEcs.System;

namespace MercurySoldier.Components
{
    struct Stick
    {
        public Entity e0;
        public Entity e1;
        public Entity stickEntity;
        public RigidBody rb0;
        public RigidBody rb1;
        public float length;
        public int width;
        public Matrix localMatrix;
        public float dx;
        public float dy;

        // For visualization
        public float strain;

        public Stick(ref Entity entity0, ref Entity entity1, ref Entity stick, int thickness = 5)
        {
            entity0.Set(new Stick());

            e0 = entity0;
            e1 = entity1;
            rb0 = entity0.Get<RigidBody>();
            rb1 = entity1.Get<RigidBody>();

            dx = rb1.X.X - rb0.X.X;
            dy = rb1.X.Y - rb0.X.Y;
            length = (float)Math.Sqrt(dx * dx + dy * dy);
            width = thickness;

            localMatrix = entity0.Get<Transform>().LocalMatrix;
            //entity0.Set(this);
            stickEntity = stick;

            strain = 0f;

            stick.Set(this);
        }

        public void updateRigidBodies()
        {
            rb0 = e0.Get<RigidBody>();
            rb1 = e1.Get<RigidBody>();
            localMatrix = e0.Get<Transform>().LocalMatrix;
        }

        public void updateEntities()
        {
            e0.Set<RigidBody>(rb0);
            //e0.Set(this);
            stickEntity.Set(this);
            e1.Set<RigidBody>(rb1);
            localMatrix = e0.Get<Transform>().LocalMatrix;
        }
    }

    
}
