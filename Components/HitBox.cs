﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using DefaultEcs;
using System;
using MercurySoldier.Systems;

namespace MercurySoldier.Components
{
    struct HitBox
    {
        // Basic AABB hitbox 

        public Vector3 min,       // bottom left corner
                       max;       // top right corner

        public float xLength,
                     yLength;

        public float width, height;

        public Rectangle hitBox;

        // if circular shape
        public float radius;
        public bool isCircle;

        public HitBox (Entity Object, int type) 
        {
            width = Object.Get<SpriteRenderer>().Texture.Width;
            height = Object.Get<SpriteRenderer>().Texture.Height;

            xLength = width / 2; 
            yLength = height / 2;  

            Vector3 localPosition = Object.Get<Transform>().LocalMatrix.GetTranslation();

            min = new Vector3(localPosition.X - xLength, localPosition.Y + yLength, 0); 
            max = new Vector3(localPosition.X + xLength, localPosition.Y - yLength, 0); 

            float x = localPosition.X - xLength;
            float y = localPosition.Y - yLength;

            hitBox = new Rectangle((int)x, (int)y, (int)width, (int)height);

            if(type == 1) //Ball
            {
                radius = xLength;
                isCircle = true;
            }
            else
            {
                radius = 0f;
                isCircle = false;
            }
        }

        public HitBox(Stick stick)
        {
            width = stick.length;
            height = stick.width;

            xLength = width / 2;
            yLength = height / 2;

            var pos3D = stick.localMatrix.GetTranslation();
            var pos2D = new Vector2(pos3D.X, pos3D.Y);

            min = new Vector3(pos2D.X - xLength, pos2D.Y + yLength, 0);
            max = new Vector3(pos2D.X + xLength, pos2D.Y - yLength, 0);

            Vector2 edge = new Vector2(stick.rb1.X.X, stick.rb1.X.Y) - new Vector2(stick.rb0.X.X, stick.rb0.X.Y);

            // calculate angle to rotate Hitbox
            float angle = (float) Math.Atan2(edge.Y, edge.X);

            hitBox = new Rectangle((int)pos2D.X, (int)pos2D.Y, (int)edge.Length(), stick.width);

            radius = 0f;
            isCircle = false;
        }


    }
}
