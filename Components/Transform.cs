﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

namespace MercurySoldier.Components
{
    public class Transform
    {
        public DefaultEcs.Entity Owner;
        public Matrix LocalMatrix = Matrix.Identity;
        public Transform Parent;
        public List<Transform> Children;

        public Matrix GetGlobalMatrix()
        {
            Transform cur = this;
            var chain = new List<Transform>();

            while (cur != null)
            {
                chain.Add(cur);
                cur = cur.Parent;
            }

            Matrix accum = Matrix.Identity;

            for (int i = chain.Count - 1; i >= 0; i--)
            {
                accum = chain[i].LocalMatrix * accum;
            }

            return accum;
        }        
    }
}
