﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace MercurySoldier.Components
{
    struct SpriteRenderer
    {
        public enum DrawMode
        {
            Simple,
            Tiled
        }

        public Texture2D Texture;
        public Vector2 Center;
        public Color Tint;
        public Point Size;
        public float Layer; // needs to be set manually and be between 0.0 and 1.0

        public SpriteRenderer(Texture2D tex) : this(tex, tex.Bounds.Size, Color.White) { }    
        
        public SpriteRenderer(Texture2D tex, Color tint) : this(tex, tex.Bounds.Size, tint) { }        

        public SpriteRenderer(Texture2D tex, Point size, Color tint, DrawMode drawMode = DrawMode.Simple)
        {            
            Center = new Vector2(size.X / 2f, size.Y / 2f);
            Tint = tint;
            Size = size;            
            Layer = 0;

            Texture = drawMode switch
            {
                DrawMode.Tiled => TiledTexture(tex, size),
                _ => tex,
            };
        }

        public static Texture2D TiledTexture(Texture2D source, Point size)
        {
            var srcPixels = new Color[source.Width * source.Height];
            source.GetData(srcPixels);

            var tiledPixels = new Color[size.X * size.Y];
            for (int y = 0; y < size.Y; y++)
            {
                for (int x = 0; x < size.X; x++)
                {
                    tiledPixels[size.X * y + x] = srcPixels[source.Width * (y % source.Height) + (x % source.Width)];
                }
            }

            var tiledTexture = new Texture2D(source.GraphicsDevice, size.X, size.Y);
            tiledTexture.SetData(tiledPixels);
            return tiledTexture;
        }
    }
}
