﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using DefaultEcs;
using System;
using MercurySoldier.Systems;

using CenterSpace.NMath.Core;

using DefaultEcs.System;

namespace MercurySoldier.Components
{
    struct Projection
    {
        public float value;
        public int number;
        public bool isEnd;

        public Entity entity;

        public Projection(ref Entity _entity, int counter, string axis, bool end)
        {
            entity = _entity;
            float b = 0f;
            float offset = 0f;

            if (axis == "x")
            {
                b = _entity.Get<HitBox>().hitBox.Left;
                offset = _entity.Get<HitBox>().hitBox.Width;
            }
            else if(axis == "y")
            {
                b = _entity.Get<HitBox>().hitBox.Top;
                offset = _entity.Get<HitBox>().hitBox.Height;
            }
            

            if (!end)
            {
                value = b;
                isEnd = false;
            }
            else
            {
                value = b + offset;
                isEnd = true;
            }

            number = counter;
        }
    }
    
    }
