﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Microsoft.Xna.Framework;

namespace MercurySoldier.Components
{
    struct Particle
    {
        public float M;
        public Vector2 Pos;
        public Vector2 Vel;
        public Vector2 FAcc;

        public override string ToString()
        {
            return "- Particle -\n" +
                $"Mass: {M}\n" +
                $"Pos: {Pos}\n" +
                $"Vel: {Vel}\n" +
                $"FAcc: {FAcc}\n";             
        }
    }

    class ParticleCollection
    {
        public Particle[] Particles = new Particle[0];
        public const int STATE_DIM = 4;
        public Color Color = Color.Blue;
        public bool StickTogether = false;

        public float[] GetState()
        {
            return Particles
                .Select(p => new float[] 
                { 
                    p.Pos.X, p.Pos.Y, 
                    p.Vel.X, p.Vel.Y,
                })
                .SelectMany(p => p)
                .ToArray();
        }

        public void ApplyState(float[] state)
        {            
            for (int particleIndex = 0; particleIndex < Particles.Length; particleIndex++)
            {
                int i = particleIndex * STATE_DIM;
                Particles[particleIndex].Pos.X = state[i + 0];
                Particles[particleIndex].Pos.Y = state[i + 1];
                Particles[particleIndex].Vel.X = state[i + 2];
                Particles[particleIndex].Vel.Y = state[i + 3];
            }
        }
        
        public float[] Derivative(float[] initialState, Action<Particle[]> ApplyForces)
        {            
            ZeroForces();
            ApplyForces(Particles);
            var derivative = new float[initialState.Length]; // float[])initialState.Clone();
            for (int i = 0; i < initialState.Length; i += STATE_DIM)
            {
                int particleIndex = i / STATE_DIM;
                var p = Particles[particleIndex];

                // pos += vel
                derivative[i + 0] = initialState[i + 2];
                derivative[i + 1] = initialState[i + 3];

                // vel += facc / m
                derivative[i + 2] = p.FAcc.X / p.M;
                derivative[i + 3] = p.FAcc.Y / p.M;
            }
            // Console.WriteLine(string.Join(';', initialState) + " => " + string.Join(';', derivative));
            return derivative;
        }

        public void ZeroForces()
        {
            for (int i = 0; i < Particles.Length; i++)
            {
                Particles[i].FAcc = Vector2.Zero;                
            }
        }

        public void ZeroVelocities()
        {
            for (int i = 0; i < Particles.Length; i++)
            {
                Particles[i].Vel = Vector2.Zero;
            }
        }

        public Vector2 AveragePosition()
        {            
            return Particles.Select(p => p.Pos).Aggregate((carry, pos) => carry + pos) / Particles.Length;
        }            
        
    }
}
