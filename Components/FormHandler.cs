﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace MercurySoldier.Components
{
    public enum Shape : int
    {
        Actor,
        Ball,
        Cloud,
        Cone
    }

    struct FormHandler
    {
        public int currentForm;
        public static Dictionary<Shape, Texture2D> FormTextures;

        public FormHandler(int Form)
        {
            currentForm = Form;
        }
    }
}
