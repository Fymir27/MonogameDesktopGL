﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using DefaultEcs;
using System;
using MercurySoldier.Systems;

namespace MercurySoldier.Components
{
    struct RigidBody
    {
        // Constant quantities
        public float Mass,        // mass M
                     width,       // width of body
                     height;      // height of body

        public Matrix Ibody,      // I_body
                      Ibodyinv;   // I_body^(-1)
        public float momentOfInertia; 

        // State variables
        public Vector3 X;         // x(t) ... position
        public Vector3 oldX;      // previous x(t) ... position
        public Quaternion q;      // q(t) to get R(t)
        public Vector3 P,         // P(t) ... linear momentum
                       L;         // L(t) ... angular momentum

        // Derived quantities
        public Matrix Iinv,       // I^(-1)(t)
                      R;          // R(t) ... orientation
        public Vector3 v;         // v(t) ... linear velocity
        public float omega;       // w(t) ... angular velocity


        // Computed quantities
        public Vector3 force;     // F(t)
        public float   torque;    // T(t)
        public Vector3 J;         // Impuls

        // Temporary quantities
        public double time;       // holds t0

        // Mass Spring System
        // variables
        public bool pinned;       // if true no physics are applied to the body

        // Testing variables
        public Vector3 pointOfIntersection;
        public bool intersects;


        public RigidBody(float mass)
        {
            Matrix zeroes = new Matrix(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            Mass = mass;
            width = 0f;
            height = 0f;
            Ibody = zeroes;
            Ibodyinv = zeroes;
            momentOfInertia = 0f;

            X = Vector3.Zero;
            oldX  = Vector3.Zero;
            q = new Quaternion(0, 0, 0, 0);
            P = Vector3.Zero;
            L = Vector3.Zero;

            Iinv = zeroes;
            R = zeroes;
            v = Vector3.Zero;
            omega = 0f;

            force = Vector3.Zero;
            torque = 0f;
            J = Vector3.Zero;

            time = 0.0;

            pinned = false;

            pointOfIntersection = new Vector3(0, 0, 0);
            intersects = false;
        }

        public void SetInputForce(Vector3 force)
        {
            this.force += force;
        }

        public void SetIntersectionVariables(bool didIntersect, Vector3 point)
        {
            intersects = didIntersect;
            pointOfIntersection = point;
        }
    }
}
